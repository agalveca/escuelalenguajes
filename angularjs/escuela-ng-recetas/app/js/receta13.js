/**
 * Created by agc on 29/01/14.
 */

var app = angular.module("MyApp", []);

app.directive("myWidget", function() {
    return {
        restrict: "E",
        transclude: true,
        template: "<h3>Heading</h3><div ng-transclude></div> "
    };
});
