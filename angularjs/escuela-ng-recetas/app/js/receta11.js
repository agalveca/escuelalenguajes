/**
 * Created by agc on 27/01/14.
 */

var app = angular.module("MyApp", []);

app.directive("myWidget", function() {
    var linkFunction = function(scope, element, attributes) {
        var paragraph = element.children()[0];
        $(paragraph).on("click", function() {
            $(this).css({ "background-color": "red" });
        });
    };

    return {
        restrict: "E", // la directiva puede usarse como elemento no como atributo
        link: linkFunction
    };
});
