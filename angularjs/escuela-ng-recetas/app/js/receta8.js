var app = angular.module("MyApp", []);

app.factory("UserService", function() {
    var users = ["Peter", "Daniel", "Nina"];

    return {
        all: function() {
            return users;
        },
        first: function() {
            return users[0];
        },

        second: function() {
            return users[1];
        }
    };
});

app.controller("MyCtrl", function($scope, UserService) {
    $scope.users = UserService.all();
});

app.controller("AnotherCtrl", function($scope, UserService) {
    $scope.firstUser = UserService.first();
});

// Para evitar problemas en la minificacion

app.controller("CtrlConAnotacion", ["$scope","UserService", function($scope, UserService) {
    $scope.secondUser = UserService.second();
}]);