<html>
<head>
    <?php echo $this->Html->script('angular.js',array('block'=>'scriptBottom')); ?>

</head>
<body ng-app >

<div ng-controller="MyCtrl">
    <p ng-init="incrementValue(5)">{{value}}</p>
    <p>{{getIncrementedValue()}}</p>
    <input type="text" ng-model="nombre" placeholder="Dime cómo te llamas">
    <p>{{saludo}}</p>
</div>

<div ng-controller="MyCtrl">
    <p>Aquí el scope es diferente, se vuelve a inicializar el controlador</p>
    <p>{{getIncrementedValue()}}</p>
</div>

<script>
    function MyCtrl($scope) {
        $scope.value = 1;

        $scope.incrementValue = function(value) {
            $scope.value += 1;
        };

        $scope.getIncrementedValue = function() {
            return $scope.value + 1;
        };

        $scope.nombre = "";

        $scope.$watch("nombre", function(newValue, oldValue) {
            if (newValue.length > 0) {
                $scope.saludo = "Saludos " + newValue;
            }
        });
    }
</script>

</body>
</html>
