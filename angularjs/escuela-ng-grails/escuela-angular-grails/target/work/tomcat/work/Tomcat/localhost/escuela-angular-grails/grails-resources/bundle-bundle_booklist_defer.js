var
//the HTTP headers to be used by all requests
    httpHeaders,
//the message to be shown to the user
    message,
    as = angular.module('myApp', ['ngResource', 'ngRoute', 'myApp.filters', 'myApp.services', 'myApp.directives', 'myApp.controllers']);

as.value('version', '1.0.7');

as.config(function ($routeProvider, $httpProvider) {
    $routeProvider
        .when('/books', {templateUrl: 'app/partials/books.html', controller: 'BookListCtrl'})
        .when('/new', {templateUrl: 'app/partials/new.html', controller: 'NewBookCtrl'})
        .when('/edit/:id', {templateUrl: 'app/partials/edit.html', controller: 'EditBookCtrl'})
        .otherwise({redirectTo: '/'});
//        $httpProvider.defaults.useXDomain = true;
//        delete $httpProvider.defaults.headers.common["X-Requested-With"];
});

as.config(function ($httpProvider) {


    //configure $http to catch message responses and show them
    $httpProvider.responseInterceptors.push(
        function ($q) {
            console.log('call response interceptor and set message...');
            var setMessage = function (response) {
                //if the response has a text and a type property, it is a message to be shown
                //console.log('@data'+response.data);
                if (response.data.message) {
                    message = {
                        text: response.data.message.text,
                        type: response.data.message.type,
                        show: true
                    };
                }
            };
            return function (promise) {
                return promise.then(
                    //this is called after each successful server request
                    function (response) {
                        setMessage(response);
                        return response;
                    },
                    //this is called after each unsuccessful server request
                    function (response) {
                        setMessage(response);
                        return $q.reject(response);
                    }
                );
            };
        });

    //configure $http to show a login dialog whenever a 401 unauthorized response arrives
    $httpProvider.responseInterceptors.push(
        function ($rootScope, $q) {
            console.log('call response interceptor...');
            return function (promise) {
                return promise.then(
                    //success -> don't intercept
                    function (response) {
                        console.log('dont intercept...');
                        return response;
                    },
                    //error -> if 401 save the request and broadcast an event
                    function (response) {
                        console.log('execute interceptor, response@' + response.status);
                        if (response.status === 401) {
                            console.log('catching http status:401');
                            var deferred = $q.defer(),
                                req = {
                                    config: response.config,
                                    deferred: deferred
                                };
                            $rootScope.requests401.push(req);
                            $rootScope.$broadcast('event:loginRequired');
                            return deferred.promise;
                        }
                        return $q.reject(response);
                    }
                );
            };
        });
    httpHeaders = $httpProvider.defaults.headers;
    //console.log('http headers:'+ httpHeaders);
});

as.run(function ($rootScope, $http, $location, base64) {
    //make current message accessible to root scope and therefore all scopes
    $rootScope.message = function () {
        return message;
    };

    /**
     * Holds all the requests which failed due to 401 response.
     */
    $rootScope.requests401 = [];

    $rootScope.$on('event:loginRequired', function () {
        console.log('fire event:loginRequired');
        //  $('#login').modal('show');
        $location.path('/login');
    });

    /**
     * On 'event:loginConfirmed', resend all the 401 requests.
     */
    $rootScope.$on('event:loginConfirmed', function () {
        var i,
            requests = $rootScope.requests401,
            retry = function (req) {
                $http(req.config).then(function (response) {
                    req.deferred.resolve(response);
                });
            };

        for (i = 0; i < requests.length; i += 1) {
            retry(requests[i]);
        }
        $rootScope.requests401 = [];

        $location.path('/');
    });

    /**
     * On 'event:loginRequest' send credentials to the server.
     */
    $rootScope.$on('event:loginRequest', function (event, username, password) {
        //            httpHeaders.common['Authorization'] = 'Basic ' + base64.encode(username + ':' + password);
        //            $http.get('action/user').success(function (data) {
        //                $rootScope.user = data;
        //                $rootScope.$broadcast('event:loginConfirmed');
        //            });
        console.log('fire event: loginRequest. @event,' + event + ', username @' + username + ', password@' + password);
        httpHeaders.common['Authorization'] = 'Basic ' + base64.encode(username + ':' + password);
        $http.get($rootScope.appUrl + '/users/login.json')
            .success(function (data) {
                console.log('login data @' + data);
                $rootScope.user = data.user;
                $rootScope.$broadcast('event:loginConfirmed');
            });

    });

    /**
     * On 'logoutRequest' invoke logout on the server and broadcast 'event:loginRequired'.
     */
    $rootScope.$on('event:logoutRequest', function () {
        $http.get($rootScope.appUrl + '/users/logout.json')
            .success(function (data) {
                httpHeaders.common['Authorization'] = null;
            });

    });
});



var as = angular.module('myApp.controllers', []);

as.controller('AppCtrl', function ($scope, $rootScope, $http, i18n, $location) {
    $scope.language = function () {
        return i18n.language;
    };
    $scope.setLanguage = function (lang) {
        i18n.setLanguage(lang);
    };
    $scope.activeWhen = function (value) {
        return value ? 'active' : '';
    };

    $scope.path = function () {
        return $location.url();
    };

//        $scope.login = function() {
//            $scope.$emit('event:loginRequest', $scope.username, $scope.password);
//            //$location.path('/login');
//        };

    $scope.logout = function () {
        $rootScope.user = null;
        $scope.username = $scope.password = null;
        $scope.$emit('event:logoutRequest');
        $location.url('/');
    };

    $rootScope.appUrl = "http://192.168.1.79:8080/escuela-angular-grails/";

});

as.controller('BookListCtrl', function ($scope, $rootScope, $http, $location) {
    var load = function () {
        console.log('call load()...');
        $scope.url= $rootScope.appUrl + '/books.json';
        $scope.url="Hll";
        $http.get($rootScope.appUrl + '/books.json')
            .success(function (data, status, headers, config) {
                $scope.books = data;
                angular.copy($scope.books, $scope.copy);
            });
    }

    load();

    $scope.addBook = function () {
        console.log('call addBook');
        $location.path("/new");
    }

    $scope.editBook = function (index) {
        console.log('call editBook');
        $location.path('/edit/' + $scope.books[index].id);
    }

    $scope.delBook = function (index) {
        console.log('call delBook');
        var todel = $scope.books[index];
        $http
            .delete($rootScope.appUrl + '/books/' + todel.id + '.json')
            .success(function (data, status, headers, config) {
                load();
            }).error(function (data, status, headers, config) {
            });
    }

});

as.controller('NewBookCtrl', function ($scope, $rootScope, $http, $location) {

    $scope.book = {};

    $scope.saveBook = function () {
        console.log('call saveBook');
        $http
            .post($rootScope.appUrl + '/books.json', $scope.book)
            .success(function (data, status, headers, config) {
                $location.path('/books');
            }).error(function (data, status, headers, config) {
            });
    }
});

as.controller('EditBookCtrl', function ($scope, $rootScope, $http, $routeParams, $location) {

    var load = function () {
        console.log('call load()...');
        $http.get($rootScope.appUrl + '/books/' + $routeParams['id'] + '.json')
            .success(function (data, status, headers, config) {
                $scope.book = data;
                angular.copy($scope.book, $scope.copy);
            });
    }

    load();

    $scope.book = {};

    $scope.updateBook = function () {
        console.log('call updateBook');
        $http
            .put($rootScope.appUrl + '/books/' + $scope.book.id + '.json', $scope.book)
            .success(function (data, status, headers, config) {
                $location.path('/books');
            }).error(function (data, status, headers, config) {
            });
    }
});


    var as = angular.module('myApp.directives', []);

    as.directive('msg', function() {
        return {
            restrict: 'EA',
            link: function(scope, element, attrs) {
                var key = attrs.key;
                if (attrs.keyExpr) {
                    scope.$watch(attrs.keyExpr, function(value) {
                        key = value;
                        element.text($.i18n.prop(value));
                    });
                }
                scope.$watch('language()', function(value) {
                    element.text($.i18n.prop(key));
                });
            }
        };
    });

    as.directive('appVersion', ['version', function(version) {
            return function(scope, elm, attrs) {
                elm.text(version);
            };
        }]);



'use strict';

/* Filters */

angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    }
  }]);


	var as = angular.module('myApp.services', []);

    as.service('i18n', function () {
        var self = this;
        this.setLanguage = function (language) {
            $.i18n.properties({
                name: 'messages',
                path: 'i18n/',
                mode: 'map',
                language: language,
                callback: function () {
                    self.language = language;
                }
            });
        };
        this.setLanguage('en');
    });

	as.service('base64', function () {
		var keyStr = "ABCDEFGHIJKLMNOP" +
			"QRSTUVWXYZabcdef" +
			"ghijklmnopqrstuv" +
			"wxyz0123456789+/" +
			"=";
		this.encode = function (input) {
			var output = "",
				chr1, chr2, chr3 = "",
				enc1, enc2, enc3, enc4 = "",
				i = 0;

			while (i < input.length) {
				chr1 = input.charCodeAt(i++);
				chr2 = input.charCodeAt(i++);
				chr3 = input.charCodeAt(i++);

				enc1 = chr1 >> 2;
				enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
				enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
				enc4 = chr3 & 63;

				if (isNaN(chr2)) {
					enc3 = enc4 = 64;
				} else if (isNaN(chr3)) {
					enc4 = 64;
				}

				output = output +
					keyStr.charAt(enc1) +
					keyStr.charAt(enc2) +
					keyStr.charAt(enc3) +
					keyStr.charAt(enc4);
				chr1 = chr2 = chr3 = "";
				enc1 = enc2 = enc3 = enc4 = "";
			}

			return output;
		};

		this.decode = function (input) {
			var output = "",
				chr1, chr2, chr3 = "",
				enc1, enc2, enc3, enc4 = "",
				i = 0;

			// remove all characters that are not A-Z, a-z, 0-9, +, /, or =
			input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

			while (i < input.length) {
				enc1 = keyStr.indexOf(input.charAt(i++));
				enc2 = keyStr.indexOf(input.charAt(i++));
				enc3 = keyStr.indexOf(input.charAt(i++));
				enc4 = keyStr.indexOf(input.charAt(i++));

				chr1 = (enc1 << 2) | (enc2 >> 4);
				chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
				chr3 = ((enc3 & 3) << 6) | enc4;

				output = output + String.fromCharCode(chr1);

				if (enc3 != 64) {
					output = output + String.fromCharCode(chr2);
				}
				if (enc4 != 64) {
					output = output + String.fromCharCode(chr3);
				}

				chr1 = chr2 = chr3 = "";
				enc1 = enc2 = enc3 = enc4 = "";
			}
		};
	});




