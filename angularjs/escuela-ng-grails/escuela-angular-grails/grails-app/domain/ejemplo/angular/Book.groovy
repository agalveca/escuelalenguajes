package ejemplo.angular
import grails.rest.*
// ver
@Resource()
class Book {

    String title
    String author
    Double price

    static constraints = {
        title blank:false
        author blank:false
    }
}
