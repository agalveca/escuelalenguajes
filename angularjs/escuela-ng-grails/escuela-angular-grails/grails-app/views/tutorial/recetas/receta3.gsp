<html>
    <head>
        <title>Receta X</title>
        <meta name="layout" content="angular"/>
        <content tag="htmlAttrs">ng-app=""</content>
        <r:require module="core"/>
        <g:javascript src="tutorial/recetas/app3.js" />
    </head>
<body>
<div ng-controller="MyCtrl">
    <button class="btn" ng-click="toggle()">Alternar</button>
    <p ng-show="visible">Hello World!</p>
</div>
</body>
</html>
