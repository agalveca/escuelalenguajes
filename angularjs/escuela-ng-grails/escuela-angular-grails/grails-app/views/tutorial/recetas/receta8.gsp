<html>
    <head>
        <title>Receta 8</title>
        <meta name="layout" content="angular"/>
        <content tag="htmlAttrs">ng-app="MyApp"</content>
        <r:require module="core"/>
        <r:require module="app8" />

        <style>
        .nested {
            border: 1px solid red;
            margin-left: 2em;
            padding: 1em;
        }
        </style>
    </head>
<body>
<div ng-controller="MyCtrl">
    <ul ng-repeat="user in users">
        <li>{{user}}</li>
    </ul>
    <div class="nested" ng-controller="AnotherCtrl">
        First user: {{firstUser}}
    </div>
</div>
</body>
</html>
