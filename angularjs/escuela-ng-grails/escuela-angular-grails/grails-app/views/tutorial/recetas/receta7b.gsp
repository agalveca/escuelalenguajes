<html>
    <head>
        <title>Receta X</title>
        <meta name="layout" content="angular"/>
        <content tag="htmlAttrs">ng-app="MyApp"</content>
        <r:require module="core"/>
        <r:require module="app7" />

        <style>
        .nested {
            border: 1px solid red;
            margin-left: 2em;
            padding: 1em;
        }
        </style>
    </head>
<body>
<div ng-controller="MyCtrl">
    <label>Primitiva</label><input type="text" ng-model="nombre">

    <label>Objeto</label><input type="text" ng-model="usuario.nombre">

    <div class="nested" ng-controller="MyNestedCtrl">
        <label>Primitiva anidada</label> <input type="text" ng-model="nombre">

        <label>Primitiva con referencia explicita al $parent</label><input type="text" ng-model="$parent.nombre">

        <label>Objeto</label><input type="text" ng-model="usuario.nombre">
    </div>
</div>

</body>
</html>
