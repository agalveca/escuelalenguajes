<html>
    <head>
        <title>Receta X</title>
        <meta name="layout" content="angular"/>
        <content tag="htmlAttrs">ng-app="MyApp"</content>
        <r:require module="core"/>
        <!--  g:javascript src="tutorial/recetas/app5.js"  no funciona por el orden
        <script type="text/javascript" src="${resource(dir:'js', file:'tutorial/recetas/app5.js')}" ></script>   -->
        <r:require module="app5"/>

    </head>
<body>
<label for="checkbox"><input id="checkbox" type="checkbox" ng-model="visible">Marca/Desmarca</label>
<div show="visible">
    <p>Hola Mundo</p>
    <div>
        <p>El atributo ng-model asigna a la variable visible el valor del input. Como es un checkbox
        el valor es cierto/Falso</p>

        <p>La directiva ejecuta una función que establece una propiedad watch para el atributo show.
        Cuando cambia, y eso ocurre al ser un ng-model al Marcar/Desmarcar el checkbox, se invoca
        una función callback que establece la propiedad display del elemento, en función del valor
        de visible a none o vacio
        </p>

    </div>
</div>
</body>
</html>
