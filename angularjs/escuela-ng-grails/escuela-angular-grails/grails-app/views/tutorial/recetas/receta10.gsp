<html>
    <head>
        <title>Receta X</title>
        <meta name="layout" content="angular"/>
        <content tag="htmlAttrs">ng-app="MyApp"</content>
        <r:require module="core"/>
<r:script>

    var app = angular.module("MyApp", []);

    app.directive("myWidget", function() {
        var linkFunction = function(scope, element, attributes) {
            var paragraph = element.children()[0];
            $(paragraph).on("click", function() {
                $(this).css({ "background-color": "red" });
            });
        };

        return {
            restrict: "E",
            link: linkFunction
        };
    });

    app.directive("miChisme", function() {
        return {
            restrict: "E",
            template: "<p>Hello World</p>"
        };
    });

    app.directive("miChismeReemplazar", function() {
        return {
            restrict: "E",
            replace: true,
            template: "<p>Hello World</p>"
        };
    });

    app.directive("miChismePlantilla", function() {
        return {
            restrict: "E",
            replace: true,
            templateUrl: "chisme.html"
        };
    });

    app.directive("miChismeAnexar", function() {
        return {
            restrict: "E",
            transclude: true,
            template: "<div ng-transclude><h3>Heading</h3></div>"
        };
    });
</r:script>
    </head>
<body>
<my-widget>
    <p>Hola Mundo cruel</p>
</my-widget>

<mi-chisme></mi-chisme>
<mi-chisme-reemplazar></mi-chisme-reemplazar>
<mi-chisme-plantilla-nofunciona></mi-chisme-plantilla-nofunciona>

<mi-chisme-anexar>
    <p>This is my paragraph text.</p>
</mi-chisme-anexar>

</body>
</body>
</html>
