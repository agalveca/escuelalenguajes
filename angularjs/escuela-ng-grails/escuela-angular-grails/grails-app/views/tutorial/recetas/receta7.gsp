recetaX.gsp<html>
    <head>
        <title>Receta X</title>
        <meta name="layout" content="angular"/>
        <content tag="htmlAttrs">ng-app="MyApp"</content>
        <r:require module="core"/>

        <r:script>
            var app = angular.module("MyApp", []);

            app.controller("MyCtrl", function($scope) {
                $scope.nombre = "Peter";
                $scope.usuario = {
                    nombre: "Parker"
                };
            });

            app.controller("MyNestedCtrl", function($scope) {
            });
        </r:script>
    </head>
<body>
<div ng-controller="MyCtrl">
<label>Primitiva</label><input type="text" ng-model="nombre">

<label>Objeto</label><input type="text" ng-model="usuario.nombre">

<div class="nested" ng-controller="MyNestedCtrl">
    <label>Primitiva anidada</label> <input type="text" ng-model="nombre">

    <label>Primitiva con referencia explicita al $parent</label><input type="text" ng-model="$parent.nombre">

    <label>Objeto</label><input type="text" ng-model="usuario.nombre">
</div>
</div>


</body>
</html>
