<html>
    <head>
        <title>Receta 4</title>
        <meta name="layout" content="angular"/>
        <content tag="htmlAttrs">ng-app=""</content>
        <r:require module="core"/>
    </head>
<body>
Enter your name: <input type="text" ng-model="name">
<p>Hello {{name | uppercase | lowercase }}!</p>
</body>
</html>
