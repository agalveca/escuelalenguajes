modules = {
    application {
        resource url:'js/application.js'
    }

    angularjs {
        resource url:'js/angular/angular.js', disposition: 'head'
        resource url:'js/angular/angular-route.js', disposition: 'head'
        resource url:'js/angular/angular-resource.js', disposition: 'head'
    }

    app5 {
        dependsOn 'core'
        resource url :'js/tutorial/recetas/app5.js'
    }

    app7 {
        dependsOn 'core'
        resource url :'js/tutorial/recetas/app7.js'
    }

    app8 {
        dependsOn 'core'
        resource url :'js/tutorial/recetas/app8.js'
    }

    baseCss {
        //resource url:'/css/main.css'
        resource url:'app/lib/bootstrap/css/bootstrap.css'
    }

    jqueryui {
        dependsOn: 'jquery'
        resource url:'app/lib/jquery-ui.js'
    }

    booklist {
        dependsOn: 'core'
        resource url:'app/js/app.js'
        resource url:'app/js/controllers.js'
        resource url:'app/js/directives.js'
        resource url:'app/js/filters.js'
        resource url:'app/js/services.js'

    }

    core {
        dependsOn 'baseCss'
        dependsOn 'jquery, jqueryui'
        dependsOn 'angularjs,application'
        
    }

}