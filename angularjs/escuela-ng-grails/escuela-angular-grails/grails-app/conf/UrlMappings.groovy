class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.${format})?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/booklist")
        "/tutorial/${directorio}/${id}" (controller:"PaginasEstaticas")


        "500"(view:'/error')

        "/books"(resources:"book")
	}
}
