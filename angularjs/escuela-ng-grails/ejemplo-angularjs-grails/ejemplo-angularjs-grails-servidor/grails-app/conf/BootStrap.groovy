import ejemplo.angular.Book
import ejemplo.angular.seguridad.Authority
import ejemplo.angular.seguridad.Person
import ejemplo.angular.seguridad.PersonAuthority

import grails.plugin.springsecurity.SecurityFilterPosition;
import grails.plugin.springsecurity.SpringSecurityUtils;

class BootStrap {

    def init = { servletContext ->

        new Book(title:"Java Persistence with Hibernate", author:"Gavin King", price:99.00).save()
        new Book(title:"Spring Live", author:"Matt Raible", price:29.00).save()

        def person =new Person(username:"test", password:"test123")
        person.save()

        def roleUser=new Authority(authority:"ROLE_USER")
        roleUser.save()

        new PersonAuthority(person:person, authority:roleUser).save()
    }
    def destroy = {
    }
}
