modules = {
    application {
        resource url:'js/application.js'
    }

    angularjs {
        resource url:'js/lib/angular/angular.js', disposition: 'head'
        resource url:'js/lib/angular/angular-route.js', disposition: 'head'
        resource url:'js/lib/angular/angular-resource.js', disposition: 'head'
    }

    baseCss {
        //resource url:'/css/main.css'
        resource url:'css/bootstrap.css'
        resource url:'img/glyphicons-halflings.png'
    }

    jqueryui {
        dependsOn: 'jquery'
        resource url:'js/lib/jquery/jquery-ui.js'
    }

    contactos {
        dependsOn: 'core'
        resource url:'js/ng-app/contactos2/app.js'
        resource url:'js/ng-app/contactos2/controllers/ContactsEditCtrl.js'
        resource url:'js/ng-app/contactos2/controllers/ContactsIndexCtrl.js'
        resource url:'js/ng-app/contactos2/controllers/ContactsShowCtrl.js'
        resource url:'js/ng-app/contactos2/controllers/MainCtrl.js'

        resource url:'js/ng-app/contactos2/services/Contact.js'

    }

    booklist {

        dependsOn: 'core'
        resource url:'js/ng-app/booklist/app.js'
        resource url:'js/ng-app/booklist/controllers.js'
        resource url:'js/ng-app/booklist/services.js'

    }



    core {
        dependsOn 'baseCss'
        dependsOn 'jquery, jqueryui'
        dependsOn 'angularjs,application'

    }

}