class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.${format})?"{
            constraints {
                // apply constraints here
            }
        }

        "/contactos"(view:'/contactos')
        "/booklist"(view:'/booklist')
        "500"(view:'/error')
	}
}
