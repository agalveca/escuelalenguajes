import lecciones.angular.grails.Contact
class BootStrap {

    def init = { servletContext ->
        new Contact(firstname:"agc",lastname:"galve",age:50).save()
        new Contact(firstname:"napoleón",lastname:"bonaparte",age:50).save()
    }
    def destroy = {
    }
}
