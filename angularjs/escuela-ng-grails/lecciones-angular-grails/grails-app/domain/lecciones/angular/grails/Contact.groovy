package lecciones.angular.grails
import grails.rest.*

@Resource(uri='/contacts',formats=['json', 'xml'])
class Contact {

    String firstname
    String lastname
    Integer age

    static constraints = {
    }
}



