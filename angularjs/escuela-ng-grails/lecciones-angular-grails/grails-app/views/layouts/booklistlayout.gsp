<!doctype html>
<html ${raw(pageProperty(name:'page.htmlAttrs'))}>
<head>
    <title>Ejemplos Angular <g:layoutTitle default="Layout Angular" /></title>
    <r:require module="baseCss"/>
    <nav:resources/>
    <g:layoutHead/>
    <r:layoutResources/>
</head>
<body>


<g:layoutBody/>

<r:layoutResources/>
</body>
</html>