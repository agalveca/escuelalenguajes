<!DOCTYPE html>
<html lang="en" ng-app="Contacts">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contactos</title>

    <r:require module="core"/>
    <r:require module="contactos"/>
    <r:layoutResources/>


    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js" type="text/javascript"></script>
    <![endif]-->



</head>
<body ng-controller="MainCtrl">

<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="#">Contactos.</a>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="span12">
            <ng-view></ng-view>
        </div>
    </div>

    <footer>
        <p>&copy; Ejemplos Angular 2014</p>
    </footer>

</div>




<r:layoutResources/>
</body>
</html>
