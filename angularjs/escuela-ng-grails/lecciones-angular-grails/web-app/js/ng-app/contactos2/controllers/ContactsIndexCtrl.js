app.controller("ContactsIndexCtrl", function($scope, $location, Contact) {
  $scope.contacts = Contact.index();

  $scope.borrar=function(id ){
      Contact.delete({},{ 'id': id });
      $location.path("/");

  } ;

  $scope.new = function() {
    $location.path("/contacts/new");
  };
});