var app = angular.module("Contacts", ["ngResource","ngRoute"]);

var prefijo="/lecciones-angular-grails/js/ng-app/contactos2/plantillas/";

app.config(function($routeProvider, $locationProvider) {
   $locationProvider.html5Mode(true);
   //$locationProvider.hashPrefix('!');
    //Cuando html5 el redirect es a contacts y los templates con prefijo contacts
    //Cuando no html5 el redirect es a / y los templates sin prefijo contacts
    // pero entonces la carpeta debe reflejarlo


   $routeProvider
     .when("/contacts", { templateUrl: prefijo+"index.html", controller: "ContactsIndexCtrl" })
     .when("/contacts/new", { templateUrl: prefijo+"edit.html", controller: "ContactsEditCtrl" })
     .when("/contacts/:id", { templateUrl: prefijo+"show.html", controller: "ContactsShowCtrl" })
     .when("/contacts/:id/edit", { templateUrl: prefijo+"edit.html", controller: "ContactsEditCtrl" })
     .when("/contacts/:id/destroy", { templateUrl:prefijo+ "index.html", controller: "ContactsIndexCtrl" })
     .otherwise({ redirectTo: "/contacts" });
 });

