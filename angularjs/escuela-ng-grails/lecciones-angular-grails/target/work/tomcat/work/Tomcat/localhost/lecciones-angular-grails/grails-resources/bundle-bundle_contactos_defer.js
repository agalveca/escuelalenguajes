var app = angular.module("Contacts", ["ngResource","ngRoute"]);

var prefijo="/lecciones-angular-grails/js/ng-app/contactos2/plantillas/";

app.config(function($routeProvider, $locationProvider) {
   $locationProvider.html5Mode(true);
   //$locationProvider.hashPrefix('!');
    //Cuando html5 el redirect es a contacts y los templates con prefijo contacts
    //Cuando no html5 el redirect es a / y los templates sin prefijo contacts
    // pero entonces la carpeta debe reflejarlo


   $routeProvider
     .when("/contacts", { templateUrl: prefijo+"index.html", controller: "ContactsIndexCtrl" })
     .when("/contacts/new", { templateUrl: prefijo+"edit.html", controller: "ContactsEditCtrl" })
     .when("/contacts/:id", { templateUrl: prefijo+"show.html", controller: "ContactsShowCtrl" })
     .when("/contacts/:id/edit", { templateUrl: prefijo+"edit.html", controller: "ContactsEditCtrl" })
     .when("/contacts/:id/destroy", { templateUrl:prefijo+ "index.html", controller: "ContactsIndexCtrl" })
     .otherwise({ redirectTo: "/contacts" });
 });


app.controller("ContactsEditCtrl", function($scope, $routeParams, $location, Contact) {

  if ($routeParams.id) {
    $scope.contact = Contact.show({ id: $routeParams.id });
  } else {
    $scope.contact = new Contact();
  }

  $scope.submit = function() {
    console.log("submit")

    function success(response) {
      console.log("success", response)
      $location.path("/contacts");
    }

    function failure(response) {
      console.log("failure", response)

      _.each(response.data, function(errors, key) {
        _.each(errors, function(e) {
          $scope.form[key].$dirty = true;
          $scope.form[key].$setValidity(e, false);
        });
      });
    }

    if ($routeParams.id) {
      Contact.update($scope.contact, success, failure);
    } else {
      Contact.create($scope.contact, success, failure);
    }

  };

  $scope.cancel = function() {
    $location.path("/contacts/"+$scope.contact.id);
  };

  $scope.errorClass = function(name) {
    var s = $scope.form[name];
    return s.$invalid && s.$dirty ? "error" : "";
  };

  $scope.errorMessage = function(name) {
    var s = $scope.form[name].$error;
    result = [];
    _.each(s, function(key, value) {
      result.push(value);
    });
    return result.join(", ");
  };
});
app.controller("ContactsIndexCtrl", function($scope, $location, Contact) {
  $scope.contacts = Contact.index();

  $scope.borrar=function(id ){
      Contact.delete({},{ 'id': id });
      $location.path("/");

  } ;

  $scope.new = function() {
    $location.path("/contacts/new");
  };
});
app.controller("ContactsShowCtrl", function($scope, $routeParams, Contact) {
  $scope.contact = Contact.show({ id: $routeParams.id });
});
app.controller("MainCtrl", function($scope) {
  $scope.ayuda="Cargada"
});
app.factory("Contact", function($resource) {
  return $resource("/lecciones-angular-grails/contacts/:id", { id: "@id" },
    {
      'create':  { method: 'POST' },
      'index':   { method: 'GET', isArray: true },
      'show':    { method: 'GET', isArray: false },
      'update':  { method: 'PUT' },
      'destroy': { method: 'DELETE' }
    }
  );
});

