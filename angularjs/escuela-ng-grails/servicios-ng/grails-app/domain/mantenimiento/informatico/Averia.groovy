package mantenimiento.informatico
import servicios.Usuario
import grails.rest.*


class Averia {


    Date dateCreated
    Date lastUpdated

    Usuario emisor
    Ubicacion ubicacion
    Equipo equipo

    String asunto
    String texto
    String tipo
    String caracterAviso
    Integer tiempoEmpleado
    String estado

    String toString(){
        emisor.apellido1+ " "+ubicacion?.nombre +" "+ equipo?.codigo+" "+asunto+ " "+texto+" "+tipo+ " "+caracterAviso+ " "+tiempoEmpleado+ " "+estado
    }

    static constraints = {

        asunto(maxSize: 255)
        texto(maxSize: 3000)
        ubicacion(nullable: true)
        equipo(nullable: true)

        estado(nullable: true)
        asunto(blank: false, nullable: false)
    }
}