package mantenimiento.informatico
import grails.rest.*
import servicios.Usuario


class Ubicacion {

    String nombre
    String tipo
    String identificador
    Boolean restringida

    static hasMany = [equipos : Equipo]

    static mapping = {
        equipos  sort: 'codigo'


    }



    static constraints = {
        identificador(blank: false, maxSize: 25)
        nombre(blank: false, maxSize: 100)
        tipo(inList:['Departamento','Aula','Despacho','Oficina','Sala de Profesores'],blank: false)
        restringida(null:true)

    }

    String toString(){
        "$identificador "
    }
}


