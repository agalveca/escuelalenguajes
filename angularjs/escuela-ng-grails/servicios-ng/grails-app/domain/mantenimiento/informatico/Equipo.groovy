package mantenimiento.informatico

import grails.rest.*


class Equipo {

    String codigo
    String descripcion
    String tipo
    String marca
    String caracteristicas
    String descripcion_ubicacion
    String modelo
    String memoria
    String discoDuro
    String red
    String anoDeCompra

    //static belongsTo = Ubicacion
    static belongsTo = [ubicacion :Ubicacion]       //tiene un campo ubicacion_id

    static constraints = {
        codigo(maxSize:50)
        descripcion(maxSize:200,nullable:true)
        tipo(inList:['Ordenador','Impresora','Portatil','Proyector','Escaner','Impresora color','Fotocopiadora','Ploter','Pizarra digital','Disco duro','Pantalla'],nullable:false)
        marca(nullable:true)
        caracteristicas(maxSize:1000,nullable:true)
        descripcion_ubicacion(nullable:false)
        modelo(nullable:true)
        memoria(nullable:true)
        discoDuro(nullable:true)
        red(nullable:true)
        anoDeCompra(nullable:true)
    }

    String toString(){

        "$codigo $descripcion_ubicacion "

    }

    /* static mapping = {
         autoTimestamp true
     }  */

}

