package servicios

// aplicacion nombre de la aplicacion a la que se aplica el rol
// nombre nombre del rol (usuario, administrador,...)
// usuario  usuario al que se aplica el rol, en la tabla se guarda como usuario_id
import grails.rest.*

@Resource(uri='/roles',formats=['json'])
class Rol {

    String aplicacion
    String nombre
    Usuario usuario

    static constraints = {
        aplicacion()
        nombre()
    }
}
