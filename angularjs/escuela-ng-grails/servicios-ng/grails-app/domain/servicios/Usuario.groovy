package servicios

import mantenimiento.informatico.Ubicacion
import grails.rest.*

@Resource(uri='/usuarios',formats=['json'])
class Usuario {




    static hasMany = [ubicaciones: Ubicacion]
    static hasOne = [ rol:Rol]


    //,enviados:servicios.mensajes.Cabecera,recibidos:servicios.mensajes.Cabecera]
    //static mappedBy=[enviados:'emisor',recibidos:'receptor']

    String apellido1
    String apellido2
    String nombre
    String user
    String password





    static constraints = {
        apellido1()
        apellido2()
        nombre()
        user()
        password()
        rol(nullable:true)


    }

    String toString(){
        "$nombre  $apellido1 $apellido2"
    }

    /* static mapping = {
        autoTimestamp true
    }  */
}







