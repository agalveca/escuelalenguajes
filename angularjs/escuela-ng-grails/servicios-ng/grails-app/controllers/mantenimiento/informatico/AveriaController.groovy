package mantenimiento.informatico



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AveriaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Averia.list(params), model:[averiaInstanceCount: Averia.count()]
    }

    def show(Averia averiaInstance) {
        respond averiaInstance
    }

    def create() {
        respond new Averia(params)
    }

    @Transactional
    def save(Averia averiaInstance) {
        if (averiaInstance == null) {
            notFound()
            return
        }

        if (averiaInstance.hasErrors()) {
            respond averiaInstance.errors, view:'create'
            return
        }

        averiaInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'averiaInstance.label', default: 'Averia'), averiaInstance.id])
                redirect averiaInstance
            }
            '*' { respond averiaInstance, [status: CREATED] }
        }
    }

    def edit(Averia averiaInstance) {
        respond averiaInstance
    }

    @Transactional
    def update(Averia averiaInstance) {
        if (averiaInstance == null) {
            notFound()
            return
        }

        if (averiaInstance.hasErrors()) {
            respond averiaInstance.errors, view:'edit'
            return
        }

        averiaInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Averia.label', default: 'Averia'), averiaInstance.id])
                redirect averiaInstance
            }
            '*'{ respond averiaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Averia averiaInstance) {

        if (averiaInstance == null) {
            notFound()
            return
        }

        averiaInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Averia.label', default: 'Averia'), averiaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'averiaInstance.label', default: 'Averia'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
