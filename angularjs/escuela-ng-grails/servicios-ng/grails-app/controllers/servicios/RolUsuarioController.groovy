package servicios

class RolUsuarioController extends grails.rest.RestfulController {

    static responseFormats = ['json']

    RolUsuarioController() {
        super(Usuario)
    }

    @Override
    def index() {


        // filtramos por el nombre de rol administrador usuario
        def nombrerol= params.rolId
        def equipos=  Usuario.findAll {
            rol.nombre ==  nombrerol
        }

        respond equipos

    }

    @Override
    def save() {
         def usuarioInstance= new Usuario(params)
        // habría que crear el rol y asociarlo
        if (usuarioInstance.hasErrors()) {
            respond usuarioInstance.errors, view:'create'
            return
        }

        usuarioInstance.save flush:true

        //respond usuarioInstance, [status: CREATED]
          respond usuarioInstance, [status: CREATED]

    }





}
