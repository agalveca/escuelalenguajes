class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.${format})?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')

        "/usuarios"(resources:"usuario")

        "/equipos"(resources:"equipo")

        "/averias"(resources:"averia")

        "/roles"(resources:"rol") {
            "/usuarios"(resources:"usuario")
        }

        "/ubicaciones"(resources:"ubicacion"){
            "/equipos"(resources:"equipo")
        }




	}
}
