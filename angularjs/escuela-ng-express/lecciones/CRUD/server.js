var express = require('express'),

    Db = require('mongodb').Db,

    Connection = require('mongodb').Connection,

    Server = require('mongodb').Server,

    cors = require('cors');

var app = express();

app.configure(function () {

    app.use(express.bodyParser());

    app.use(cors());

    app.use(express.static(__dirname+'/app/' ));

});

app.listen(3000);

console.log('Listening on port 3000...');