var app=angular.module('appindice',[]);

app.controller("VisibilidadController",function ($scope) {

    $scope.mensaje=function() {

        if ($scope.showDetails) return "Ocultar";
        else return "Mostrar";
    };

    $scope.etiqueta=function(capitulo) {


    };

    $scope.capituloSeleccionadoQ=function(capactual){
        if ($scope.capitulo==capactual) return true;
        else
        return false;


    };

    $scope.seleccionarCapitulo=function(capactual) {
        $scope.capitulo= capactual;
    }



    $scope.capitulo3=[
        {ruta:'cap3/httpbasics.html',etiqueta:'Http básico'},
        {ruta:'cap3/jsonp.html',etiqueta:'Jsonp'},
        {ruta:'cap3/cors.html',etiqueta:'Cors'},
        {ruta:'cap3/resource.html',etiqueta:'Resource'},
        {ruta:'cap3/customResource.html',etiqueta:'Custom Resource'}
        ];

    $scope.capitulo4=[
    {ruta:"cap4/expressionscaping.html", etiqueta:'Escapado de expresiones'},
    {ruta:"cap4/showSecret.html",        etiqueta:'Ocultar y mostrar'},
    {ruta:"cap4/repeater.html",          etiqueta:'Repetir y repetir'},
    {ruta:"cap4/listAndDetails.html",    etiqueta:'Listas y detalles'},
    {ruta:"cap4/arrayFilters.html",      etiqueta:'Filtros en array'},
    {ruta:"cap4/highlight.html",         etiqueta:'Resaltado'},
    {ruta:"cap4/filtersPerf.html",       etiqueta:'Filtros perf'},
    {ruta:"cap4/filtersStability.html",  etiqueta:'Filtros estabilidad'}];

    $scope.capitulo5=[
        {ruta:'cap5/index_ngmodel.html',etiqueta:'NG_MODEL'},
        {ruta:'cap5/index_editform.html',etiqueta:'Forma de edición'},
        {ruta:'cap5/index_text_input_directive.html',etiqueta:'Text Input directiva'},
        {ruta:'cap5/index_checkbox_radio.html',etiqueta:'Check Box'},
        {ruta:'cap5/index_select_simple.html',etiqueta:'Select simple'},
        {ruta:'cap5/index_complex_select.html',etiqueta:'Select complex'},
        {ruta:'cap5/index_select_empty.html',etiqueta:'Select con opciones nulas'},
        {ruta:'cap5/index_select_acoplado.html',etiqueta:'Selects acoplados'},
        {ruta:'cap5/index_select_acoplado_underscore.html',etiqueta:'Selects acoplados con underscore'},
        {ruta:'cap5/index_errores_validacion.html',etiqueta:'Errores de validacion'},
        {ruta:'cap5/index_no_validate.html',etiqueta:'No validar '},
        {ruta:'cap5/index_subforma_compartida.html',etiqueta:'Subforma compartida'},
        {ruta:'cap5/index_subforma_compartida_externa.html',etiqueta:'Subforma compartida externa'},
        {ruta:'cap5/index_repetir_subformas.html',etiqueta:'Obtener información del controlador'},
        {ruta:'cap5/index_envio_directo.html',etiqueta:'Post directo a servidor'},
        {ruta:'cap5/index_envio_indirecto.html',etiqueta:'Post indirecto a servidor'},
        {ruta:'cap5/index_reset_form.html',etiqueta:'Reseteo del formulario'}


    ];

    $scope.capitulo6=[
        {ruta:'cap6/index_html5mode.html',etiqueta:'Uso de location en modo html5'},
        {ruta:'cap6/index_anchorscroll.html',etiqueta:'Gestionar scroll'},
        {ruta:'cap6/index_location_include.html',etiqueta:'Incluir plantillas según la url'},
        {ruta:'cap6/index_routing_basics.html',etiqueta:'Uso basico de routing'}];


    $scope.capitulo7=[
        {ruta:'cap7/index_sanitize.html',etiqueta:'Directivas sanitize'},
        {ruta:'cap6/index_anchorscroll.html',etiqueta:'Gestionar scroll'},
        {ruta:'cap6/index_location_include.html',etiqueta:'Incluir plantillas según la url'},
        {ruta:'cap6/index_routing_basics.html',etiqueta:'Uso basico de routing'}];




});
