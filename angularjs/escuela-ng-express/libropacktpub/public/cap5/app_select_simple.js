var app = angular.module('myApp', []);

app.controller('MainCtrl', function($scope) {
  $scope.myChoice = 3;
  $scope.options = [
    { name: 'uno', value: 1},
    { name: 'dos', value: 2},
    { name: 'tres', value: 3},
    { name: 'cuatro', value: 4}];
});
