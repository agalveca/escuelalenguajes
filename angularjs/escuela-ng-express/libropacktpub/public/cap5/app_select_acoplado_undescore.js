var modulo=angular.module("miAplicacion",[]);

var underscore = angular.module('underscore', []);

underscore.factory('_', function() {
    return window._; // assumes underscore has already been loaded on the page
});


modulo.controller("ControladorPrincipal", ['$scope', function ($scope){


    $scope.usuarios= [

        {nombre:"Juan"},
        {nombre:"Juanita"},
        {nombre:"Pedro"}
    ];

    $scope.telefonos= [
        {  nombre:"Juan",telefono:'j97612345'},
        {  nombre:"Juanita",telefono:'t00012345'},
        {  nombre:"Pedro",telefono:'p000xxx12345'},
        {  nombre:"Juan",telefono:'j0aaa00xxx12345'},
        {  nombre:"Juanita",telefono:'t00012345pipi'}

    ];

    $scope.usuarioActual= _.first($scope.usuarios);

    var telefonosUsuarioActual=function(){

        return  _.filter($scope.telefonos, function(infotelefono) {
            return (infotelefono.nombre === $scope.usuarioActual.nombre );

                });

    };

    $scope.telefonosUsuarioActual=telefonosUsuarioActual();

    $scope.cambiadoUsuarioActual=function() {

        $scope.telefonosUsuarioActual=telefonosUsuarioActual();
    }



}]);