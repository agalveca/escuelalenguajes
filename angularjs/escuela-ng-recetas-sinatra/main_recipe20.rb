require "sinatra"
require "sinatra/reloader" if development?
require "json"

require_relative 'bookmark'

get '/datos.json' do

content_type :json
{:key1=> "valor1" , :key2=>"valor2", :key3 =>4, :key4=>"Añadida" }.to_json



end


get '/bookmarks.json' do
  content_type :json
  [Bookmark.new("wwww.elpais.es","Diario el Pais"),Bookmark.new("wwww.abce.es","El abc")].to_json
#Bookmark.new("wwww.elpais.es","Diario el Pais..").to_json

end


get '/vuelta' do

  content_type :json
  bookmark = Bookmark.json_create ({:datos=>{:url=>"wwww.elpais.es",:descripcion=>"Diario el Pais***"}})

  bookmark.to_json
end


get '/data/posts.json' do

  content_type :json
  posts=[
    {
      :id=> 1,
      :title=> "Lorem ipsum 1"
    },
    {
          :id=> 2,
          :title=> "Lorem ipsum 2"
    }
  ]

  posts.to_json

end
