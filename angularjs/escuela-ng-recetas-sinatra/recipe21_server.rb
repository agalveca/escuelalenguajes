require 'sinatra'
require 'json'
require 'dm-core'
require 'dm-validations'
require 'dm-timestamps'
require 'dm-migrations'
require "dm-serializer"

if development?
  require 'sinatra/reloader'

end



configure :development, :production do
 # set :datamapper_url, "sqlite3://#{File.dirname(__FILE__)}/corkboard.db"
  set :datamapper_url, "sqlite3://#{Dir.pwd}/corkboard.db"
end

configure :test do
  set :datamapper_url, "sqlite3://#{File.dirname(__FILE__)}/corkboard-test.db"
end

before do
  content_type 'application/json'
end



DataMapper.setup(:default, settings.datamapper_url)
DataMapper.finalize.auto_upgrade!

class Note
  include DataMapper::Resource

  property(:id, Serial)
  property(:subject, Text, :required => true)
  property(:content, Text, :required => true)
  property(:created_at, DateTime)
  property(:updated_at, DateTime)



end

DataMapper.finalize
Note.auto_upgrade!

def jsonp(json)
  params[:callback] ? "#{params[:callback]}(#{json})" : json
end


get '/notes/:id' do
  note = Note.get(params[:id])
  halt 404 if note.nil?
  jsonp(note.to_json)
end

get '/notes/?'  do

  content_type :json
  notas=Note.all #.collect {|note| note.to_json}
  #notas=Note.all
  notas.to_json
end


put '/notes/?' do
  # Request.body.read is destructive, make sure you don't use a puts here.
  data = JSON.parse(request.body.read)

  # Normally we would let the model validations handle this but we don't
  # have validations yet so we have to check now and after we save.
  if data.nil? || data['subject'].nil? || data['content'].nil?
    halt 400
  end

  note = Note.create(
              :subject => data['subject'],
              :content => data['content'],
              :created_at => Time.now.utc,
              :updated_at => Time.now.utc)

  halt 500 unless note.save

  # PUT requests must return a Location header for the new resource
  [201, {'Location' => "/note/#{note.id}"}, jsonp(note.to_json)]
end


post '/notes/:id' do
  # Request.body.read is destructive, make sure you don't use a puts here.


  data = JSON.parse(request.body.read)


  halt 400 if data.nil?

  note = Note.get(params[:id])
  halt 404 if note.nil?

  %w(subject content).each do |key|
    if !data[key].nil? && data[key] != note[key]
      note[key] = data[key]
      note['updated_at'] = Time.now.utc
    end
  end

  halt 500 unless note.save
  jsonp(note.to_json)
end

# Remove a note entirely
#
delete '/notes/:id' do
  note = Note.get(params[:id])
  halt 404 if note.nil?

  halt 500 unless note.destroy
  204
end