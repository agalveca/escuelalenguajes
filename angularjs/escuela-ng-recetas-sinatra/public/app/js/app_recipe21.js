var app = angular.module("myApp", [ "ngResource"]);

app.factory("Note", function($resource) {
  return $resource("/notes/:id",{id:"@id"});
});

app.factory("notes",function(Note) {
    return Note.query() ;
});

app.controller("NoteIndexCtrl", function($scope, Note, notes) {

    $scope.notes= notes;


  $scope.remove = function(note) {
    Note.remove({ id: note.id }, function() {
      $scope.notes.forEach(function(n, index) {
        if (n.id == note.id) $scope.notes.splice(index, 1);
      });
    });
  };
});

app.controller("NoteShowCtrl", function($scope, Note) {
  $scope.recuperar= function(id) {

  Note.get({ id: id  }, function(data) {
    $scope.note = data;
  }
  );}
});