describe(' Controlador MyCtrl', function(){
  var scope, ctrl;

  beforeEach(module('MyApp'));

   beforeEach(inject(function($injector, $controller, $rootScope) {
    scope = $rootScope.$new();
    ctrl = $controller('MyCtrl', { $scope: scope });
  }));

  it('debería cambiar el saludo si el valor de nombre se cambia', function() {
    scope.name = "Frederik";
    scope.$digest();
    expect(scope.greeting).toBe("Greetings Frederik");
  });
});