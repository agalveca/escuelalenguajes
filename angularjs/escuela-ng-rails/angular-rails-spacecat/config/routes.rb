AngularRailsSpacecat::Application.routes.draw do

  get "screencasts/create"
  #resources :screencasts
  #resources :spacecats

  scope :api do
      get "/spacecats(.:format)" => "spacecats#index"
      get "/spacecats/:id(.:format)" => "spacecats#show"
      post "/spacecats(.:format)"  => "spacecats#create"
    end

  get '/', :to => redirect('/app/index.html')

  #get "/api/spacecats", :to => "spacecats#index"

  #get "api", :to => proc { [404, {}, ['Invalid API endpoint']] }
  #get "api/*path", :to => proc { [404, {}, ['Invalid API endpoint']] }

  #get "/*path", :to => redirect("/?goto=%{path}")

end
