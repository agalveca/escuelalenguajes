class SpacecatsController < ApplicationController

  protect_from_forgery with: :null_session

  def index
      render json: Spacecat.all
    end

    # GET /spacecat/:id
    # GET /spacecat/:id.json
    def show
      render json: Spacecat.find(params[:id])
    end

  def create
      @gato = Spacecat.new(params.permit(:name,:bio,:color,:personality,:planets))

      if @gato.save
               #render json: @gato, status: :created
        redirect_to("/")

              else
                render json: @gato.errors, status: :unprocessable_entity

              end


    end
end
