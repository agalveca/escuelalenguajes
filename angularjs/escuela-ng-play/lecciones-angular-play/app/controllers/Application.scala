package controllers

import play.api._
import play.api.mvc._

object Application extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def paginaProductos=  Action {
    Ok(views.html.paginaproductos())
  }

  def paginaEjemplo(param: String = "") = Action {
    Ok(views.html.paginaejemplo())
  }

  // los parciales los intercepta play luego deben existir
  def view1 = paginaEjemplo()
  def view2 = paginaEjemplo()

}