require "sinatra"
require 'sinatra/activerecord'
require_relative './modelos/contact'
require 'yaml'
require "sinatra/reloader" if development?
require "json"

DB_CONFIG = YAML::load(File.open('./config/database2.yml'))

#set :database, "mysql2://#{DB_CONFIG['username']}:#{DB_CONFIG['password']}@#{DB_CONFIG['host']}:#{DB_CONFIG['port']}/#{DB_CONFIG['database']}"


#set :database, "sqlite3://#{DB_CONFIG['development.database']}"
set :database, "sqlite3:///db/data.sqlite3"
set :public_folder, 'public' #no es necesario lo escribo a modo de documentacion

get "/" do
  File.read(File.join('public', 'index.html'))
    #send_file File.join(settings.public_folder, 'index.html')
    #redirect '/index.html'
    #en lugar de erb:application.erb
    # asi no se complican las rutas
end

get "/contacts" do
    redirect "/"
end

get '/api/contactos' do
  puts "llega 1"
  content_type :json
  contactos = Contact.all
  contactos.to_json

end

 get '/api/contactos/new' do
   contacto = Contact.new
   contacto.to_json


 end

get '/api/contactos/:id' do

  contacto = Contact.find(params[:id])
  contacto.to_json

end

get '/api/contactos/:id/edit' do

  contacto = Contact.find(params[:id])
  contacto.to_json
end

post '/api/contactos' do

  data = JSON.parse(request.body.read)

  if data.nil? || data['firstname'].nil? || data['lastname'].nil?   || data['age'].nil?
      halt 400
    end

    contacto = Contact.new(
                :firstname => data['firstname'],
                :lastname => data['lastname'],
                :age => data['age'],
                :created_at => Time.now.utc,
                :updated_at => Time.now.utc)

    halt 500 unless contacto.save
end

put '/api/contactos/:id' do
  puts "llega 6"

  data = JSON.parse(request.body.read)

  puts data

    if data.nil? || data['id'].nil? || data['firstname'].nil? || data['lastname'].nil?   || data['age'].nil?
        halt 400
      end

  halt 500 unless Contact.update(
                  data['id'],
                  :firstname => data['firstname'],
                  :lastname => data['lastname'],
                  :age => data['age'],
                  :created_at => Time.now.utc,
                  :updated_at => Time.now.utc)




end

# este se usa con un botón en un formaulario mandando un hidden

delete '/api/contactos/:id' do

   puts "DDDD"
   Contact.find(params[:id]).destroy

end


get '/api/contactos/:id/delete' do

   puts "DDDDxxxx"
   Contact.find(params[:id]).destroy

end




