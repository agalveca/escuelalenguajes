var app = angular.module("Contacts", ["ngResource","ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
   $locationProvider.html5Mode(true);
   //$locationProvider.hashPrefix('!');
    //Cuando html5 el redirect es a contacts y los templates con prefijo contacts
    //Cuando no html5 el redirect es a / y los templates sin prefijo contacts
    // pero entonces la carpeta debe reflejarlo
   $routeProvider
     .when("/contacts", { templateUrl: "/js/ng-app/templates/index.html", controller: "ContactsIndexCtrl" })
     .when("/contacts/new", { templateUrl: "/js/ng-app/templates/edit.html", controller: "ContactsEditCtrl" })
     .when("/contacts/:id", { templateUrl: "/js/ng-app/templates/show.html", controller: "ContactsShowCtrl" })
     .when("/contacts/:id/edit", { templateUrl: "/js/ng-app/templates/edit.html", controller: "ContactsEditCtrl" })
     .when("/contacts/:id/destroy", { templateUrl: "/js/ng-app/templates/index.html", controller: "ContactsIndexCtrl" })
     .otherwise({ redirectTo: "/contacts" });
 });

