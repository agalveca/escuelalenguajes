Leeme

La fuente de este ejemplo es

https://github.com/ashleygwilliams/angular-sinatra-spacecat

Debido a que se trata de una aplicación modular se debe ejectuar

rackup -p 9393

el puerto se elije así porque en el código lo ha usado

Migración

rake db:migrate
rake db:seed


He modificado la estructura de la aplicación

en lugar de que todo este debajo de app he usado esta estructura

public
  css
  img
  js
    lib
        angular
        jquery
    ng-app
        partials
            cat-list.html
            ...
        app.js
        ....

  index.html

  además de modificar las rutas en index.html , que son relativas a public las he tenido que
  modificar en app.js

  que TAMBIEN SON RELATIVAS A public   o a index.html


 Puede ser interesante la aplicación rails

 https://github.com/ashleygwilliams/angular-rails-spacecat
