require 'bundler'
Bundler.require

require 'sinatra/activerecord'
require './lib/spacecat'

# Se ejecuta rackup -p 9393
class SpacecatApp < Sinatra::Application

  configure do
    set :root, File.dirname(__FILE__)
    #set :public_folder, 'public/app'
    set :public_folder, 'public'
  end

  set :database, "sqlite3:///database.db"

  get '/' do
    File.read(File.join('public', 'index.html'))
  end

  get '/spacecats' do
    @spacecats = Spacecat.all

    @spacecats.to_json
  end

  get '/spacecats/:id' do
    @spacecat = Spacecat.find(params[:id])

    @spacecat.to_json
  end

  post '/spacecats' do
    @spacecat = Spacecat.create!(params)

    redirect '/'
  end

end