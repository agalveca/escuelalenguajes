require "sinatra"
require_relative "apijsondm"

set :public_folder, '.'



get "/" do

  @example_template = IO.read("views/routing.html")

  @examples = [
      {:example => "base", :label => "Base"},
      {:example => "validation", :label => "Validation"},
      {:example => "tagfilter", :label => "Tag Filter"},
      {:example => "routing", :label => "Routing",:active=>true}
    ]

  @example='routing'
  mustache :index

end



