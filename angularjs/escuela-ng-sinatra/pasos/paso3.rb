require "sinatra"
require_relative "apijsondm"

set :public_folder, '.'


get "/" do
  redirect "/example/base"
end

get "/example/:example" do
  @examples = [
    {:example => "base", :label => "Base"},
    {:example => "validation", :label => "Validation"},
    {:example => "tagfilter", :label => "Tag Filter"},
    {:example => "routing", :label => "Routing"}
  ]
  @example = params[:example]

  @examples.each do |example|
    if example[:example] == @example
      example[:active] = true
    end
  end

  @example_template = IO.read("views/#{@example}.html")

  mustache :index
end

