require "sinatra"
require_relative "apijsondm"

set :public_folder, '.'

get "/" do

  @example_template = IO.read("views/tagfilter.html")

  @examples = [
      {:example => "base", :label => "Base"},
      {:example => "validation", :label => "Validation"},
      {:example => "tagfilter", :label => "Tag Filter",:active=>true},
      {:example => "routing", :label => "Routing"}
    ]

  @example='tagfilter'
  mustache :index

end


