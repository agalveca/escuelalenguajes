require "sinatra"
require_relative "apijsondm"

set :public_folder, '.'


get "/" do

  @example_template = IO.read("views/base.html")

  @examples = [
      {:example => "base", :label => "Base",:active=>true},
      {:example => "validation", :label => "Validation"},
      {:example => "tagfilter", :label => "Tag Filter"},
      {:example => "routing", :label => "Routing"}
    ]

  @example='base'
  mustache :index

end


