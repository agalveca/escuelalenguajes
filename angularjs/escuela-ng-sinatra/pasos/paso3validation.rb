require "sinatra"
require_relative "apijsondm"

set :public_folder, '.'


get "/" do

  @example_template = IO.read("views/validation.html")

  @examples = [
      {:example => "base", :label => "Base"},
      {:example => "validation", :label => "Validation",:active=>true},
      {:example => "tagfilter", :label => "Tag Filter"},
      {:example => "routing", :label => "Routing"}
    ]

  @example='validation'
  mustache :index

end
