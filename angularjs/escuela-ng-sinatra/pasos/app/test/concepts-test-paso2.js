describe("concepts-tests.js", function() {

  describe("resource", function() {
    it("CRUDs with the server", function(done) {
      var app = angular.module("TestApp", ["ngResource"]);
      app.service("testService", function($resource) {
        expect($resource).toBeDefined();

        var Bookmark = $resource("/bookmarks/:id", {id:"@id"});
        expect(Bookmark).toBeDefined();

        new Bookmark({url:"http://angularjs.org", title:"AngularJS"}).
          $save(function(bookmark) {
            expect(bookmark.id).toBeDefined();
            expect(bookmark.id).toBeGreaterThan(0);

            Bookmark.query(function(bookmarks) {
              expect(bookmarks).toBeDefined();
              expect(bookmarks.length).toBeGreaterThan(0);

              bookmarks[bookmarks.length - 1].$delete(function() {
                done();
              });
            });
          });
      });

      app.run(function(testService) { });
      angular.bootstrap(document, ["TestApp"]);
    });

    it("allows to override default parameter", function(done) {
      var app = angular.module("TestApp", ["ngResource"]);
      app.service("testService", function($resource) {
        var Bookmark = $resource("/bookmarks/:id", {id:"@id"});
        var someId = 11;
        Bookmark.query(function(bookmarks) {
          someId = bookmarks[0].id;

        // This issues a request to /bookmarks/11
        Bookmark.get({id:someId}, function(bookmark) {
          // ...
          expect(bookmark).toBeDefined();
          expect(bookmark.id).toBe(someId);
          done();
        });

        });
      });

      app.run(function(testService) { });
      angular.bootstrap(document, ["TestApp"]);
    });
  });


});
