package functional

// Referencia de los tipos asociados a cada función
// Option.map (f: A ->  B ): Option[B]
// Option.flatMap(f: A ->  Option[B] ): Option[B])
// Option.orElse(f: A ->  B ): Option[B]
// Option.getOrElse(f: A ->  B ): B
// Option.filter(f: A ->  Boolean ): Option[A]

import groovy.transform.Canonical



@Canonical

abstract class Option<A> {
    
      A referencia

      static def Some(A a) { new Some(a)}
      static def None() { new None()}


	
	def map ( Closure f ) {   

		if ( this == None()) None()
		else
		new Some(f(referencia))

	}
    
   
	def getOrElse ( Closure f ) {  
		if ( this == None() ) f(referencia) 
		else this.referencia
	}

    
      def orElse (Closure f) {

        if (this == None()) f(referencia)
        else this

      }
    
   
	def flatMap (Closure f) {
		if ( this == None()) None()
		else
		f(referencia)
	}

    
      def filter(Closure f) {

        if (f(referencia)) this
        else None()

      }

}




class Some<A> extends Option<A>{

	protected Some(A a) {
		super(a)
	}


}


class None<A> extends Option<A>  {
    protected None() {
        super(null)
    }
}

