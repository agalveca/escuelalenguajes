package functional
import groovy.transform.Canonical


// Referencia de los tipos asociados a cada función
// Either.orElse ( f: A ->A): Either[A]
// Either.map (f: A ->  B ): Either[B]
// Either.flatMap(f : A ->Either(B) :Either(B





@Canonical

abstract  class Either<A> {
	
	A referencia

      static def Left(A a) { new Left(a)}
      static def Right(A a) { new Right(a)}

	
  	def map ( Closure f ) {   

		if ( this instanceof functional.Left) this
		else
		new Right(f(referencia))

	}
	
	def orElse (Closure f) {
		if (this instanceof functional.Left) f(referencia)
		else this

      }

      def flatMap(Closure f) {
		if (this instanceof functional.Left) this
		else f(this.referencia)

     
   }
}



class Left<A> extends Either<A> {
	
	protected Left(A a) {
		super(a)
	}
	
	
}
class Right<A> extends Either<A> {
	
	protected Right(A a) {
		super(a)
	}
	
}
