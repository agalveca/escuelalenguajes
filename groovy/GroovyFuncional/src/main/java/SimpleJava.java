public class SimpleJava{
 
    public int sumAll(int... args) {
 
        int sum = 0;
 
        for (int arg : args){
            sum += arg;
        }
 
        return sum;
    }
}