import spock.lang.Specification

import static functional.Either.Left as Left
import static functional.Either.Right as Right

 class EitherSpec extends Specification {
	
		def "Left y Right tienen los tipos adecuadados "() {
	
	  given : "Una instancia de Left y otra de Right"
	  def bien = Right(25)
	  def mal = Left("Error")
	
	  expect:
	  bien.class == functional.Right
	  (bien instanceof functional.Right) == true
	  (bien instanceof functional.Either) == true	   
	   bien.class != functional.Either
	   bien.class != functional.Left
	   
	
	  mal.class == functional.Left
	  (mal instanceof functional.Left) == true
	  (mal instanceof functional.Either) == true	   
	   mal.class != functional.Either
	   mal.class != functional.Right
	   
	  	  
		
	}
	
	def "orElse se comporta correctamente "() {
		
		given : "Una instancia de Left y otra de Right"
		  def bien = Right(25)
		  def mal = Left("Hay error")
		expect: "orElse devuelve el mismo Right"
			bien.orElse { it -> it} == Right(25) // se ignora la clausura
			mal.orElse {s -> Right("*"+s+"*")} == Right("*Hay error*")
		
	}

	def "map se comporta correctamente "() {
		
		given : "Una instancia de Left y otra de Right"
		  def bien = Right(5)
		  def mal = Left("Hay error")
		expect: "map devuelve el mismo Left y el Right con el contenido transformado"
			bien.map { it -> it*it} == Right(25) 
			mal.map {s -> Right("*"+s+"*")} == Left("Hay error")
		
	}

	def "flatmap se comporta correctamente "() {
		
		given : "Una instancia de Left y otra de Right"
		  def bien = Right(5)
		  def mal = Left("Hay error")
		expect: "map devuelve el mismo Left y el Right con el contenido transformado"
			bien.flatMap { it -> Right(it*it)} == Right(25) 
			mal.flatMap {s -> Right("*"+s+"*")} == Left("Hay error")
		
	}
	
}
