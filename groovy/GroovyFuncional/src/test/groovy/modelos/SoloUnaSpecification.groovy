import spock.lang.*
class SoloUnaSpecification extends Specification {
 
    
    def "Can sum different amount of integers"() {
 
        given:
            def instance = new SimpleJava()
 
        when:
            def result = instance.sumAll(* integers)
 
        then:
            result == expectedResult
 
        where:
            expectedResult | integers
            11             | [3, 3, 5]
            8              | [3, 5]
            254            | [2, 4, 8, 16, 32, 64, 128]
            22             | [7, 5, 6, 2, 2]
    }
 
    
    def "Can concatenate different amount of integers with a specified separator"() {
 
        given:
            def instance = new SimpleGroovy()
 
        when:
            def result = instance.concatenateAll(separator, * strings)
 
        then:
            result == expectedResult
 
        where:
            expectedResult     | separator   | strings
            'Whasup dude?'     | ' ' as char | ['Whasup', 'dude?']
            '2012/09/15'       | '/' as char | ['2012', '09', '15']
            'nice-to-meet-you' | '-' as char | ['nice', 'to', 'meet', 'you']
    }
}