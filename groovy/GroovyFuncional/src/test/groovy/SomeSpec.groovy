import spock.lang.Specification

import static functional.Option.None as None
import static functional.Option.Some as Some

 class SomeSpec extends Specification {

    def None = None()

    
       	def "Some y None tienen los tipos adecuadados "() {
	
	  given : "Una instancia de Some y otra de None"
	  def algo = Some("Hola")
	  def nada = None
	
	  expect:
	  (algo instanceof functional.Some) == true
	  (algo instanceof functional.Option) == true
	   algo.class == functional.Some
	   algo.class != functional.Option
	   algo.class != functional.None
	
	  (nada instanceof functional.Some) == false
	  (nada instanceof functional.None) == true
	   nada.class == functional.None
	  (nada instanceof functional.Option) == true
	  
		
	}

	def "Se puede instanciar un objeto Some"() {

		given : "Dado un objeto Option"
		def some = Some("Hola mundo")

		expect: "Es igual a si mismo"

		some == Some("Hola mundo")

	}
	
	

	def "Se puede usar un objeto None"() {

		given : "Dado un objeto Option"
		def some = Some("Hola mundo")

		expect: "No es igual a None"

		some != None
		None == None

	}


	def "Se puede aplicar map a un objeto Some y a un objeto None" () {

		given : "Dado un objeto Option"

		def some = Some(5)
		def cuadrado = Some(25)

		when: "Se le aplica una función"

		def resultado= some.map { el -> el*el}

		then : 

		resultado == Some(25)

		None.map { Some(it*it)} == None

		None.map { it*it} != Some("Hola Mundo") // a este lado no funciona Newifty


	}


	def "Si la función devuelve None map también" () {

		given : "Dado un objeto Option"

		def some = Some(5)

		when: "Se le aplica una función"

		def resultado= some.map { el -> null}

		then : 

		resultado == None
		resultado != Some("Hola Mundo")

		

	}

	def "getOrElse funciona con Some y con None" () {

		given : "Dado un objeto Option"

		def some = Some(5)
		def cuadrado = Some(25)

		when: "Se le aplica una función"

		def resultado= some.getOrElse { el -> el*el}

		then : 

		resultado == 5

		None.getOrElse { 5*5} == 25

		None.getOrElse { 5*5} != "Hola Mundo" 

		None.getOrElse {"Hola Mundo"} == "Hola Mundo"


	}

	def "flatMap funciona con Some y con None" () {

		given : "Dado un objeto Option"

		def some = Some(5)
		

		when: "Se le aplica una función"

		def resultado= some.flatMap { el -> Some(el*el)}

		then : 

		resultado == Some(25)

		some.flatMap { el -> None} == None

		None.flatMap { el -> Some(el*el)} == Some(null) //equivale a None

		


	}
	
	def "Filter funciona con Some "() {
		
		given : "Dado un objeto Option"

		def impar = Some(5)
		
		def par = Some(4)
		
		when: "Se le aplica una filtro"

		def parfiltrado= par.filter { el -> el % 2 ==0}
		def imparfiltrado= impar.filter { el -> el % 2 ==0}
		
		then: "El filtro funciona como se espera"
		
		parfiltrado == par
		imparfiltrado == None
		
		
	}
	
	def "Aplicando collect a una lista groovy" () {
		
		given: "Una lista groovy"
		 def lista = [ 1,2,null,3,"Hola"]
		
		when: "Se aplica Some a los elementos de la lista"
		
		 def nuevalista = lista. collect { Some(it)}
		
		then: "Se obtiene una lista de Somes"
		
		nuevalista == [Some(1),Some(2),None,Some(3),Some("Hola")]
		
		
	}
	
	def "Encadenando maps  " () {
		
		given:" Un some "
		def algo = Some(5)
		when: "Se le aplican varias funciones "
	        def compuesto= algo.map { 2*it}.map {2+it}
	        def anulado= algo.map { null }.map {2*it}
		then: "El resultado es el adecuado "
		compuesto == Some(12)
		compuesto.getOrElse() == 12
		anulado == None
		anulado.getOrElse {1000} == 1000
		
		
	}
	
	def "Encadenando flatMaps" () {
		
		given:" Un some "
		def algo = Some(5)
		when: "Se le aplican varias funciones "
	        def compuesto= algo.flatMap { Some(2*it)}.flatMap {Some(2+it)}
	        def anulado= algo.flatMap { None }.flatMap {Some(2*it)}
		then: "El resultado es el adecuado "
		compuesto == Some(12)
		compuesto.getOrElse() == 12
		anulado == None
		anulado.getOrElse {1000} == 1000
		
	}



	


}