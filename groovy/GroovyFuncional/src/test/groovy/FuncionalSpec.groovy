
import spock.lang.Specification

class Person {
    String username
    String email
}

 class FuncionalSpec extends Specification {

	def "Ejemplo sencillo de inject"() {

	    given:
	      def rango =(1..4)
	    when: "Aplicamos inject"
	      def plegado= rango.inject(0) { result, i -> result + i }

	    then:

		plegado == 10
	

	}

	 def "Ejemplo de inject con clases" () {



	    given:
	     def persons = [
		new Person(username:'mrhaki', email: 'email@host.com'),
		//new Person(username:'hubert', email: 'other@host.com')
	     ]

             def persona = new Person()
             
             persona.with {
             username= 'hubert'
             email= 'other@host.com'
   
             }
             
             persons= persons<< persona

	    when: "Aplicamos inject"
	     def plegado = persons.inject([:]) { result, person ->
		                  result[person.username] = person.email
		                  result }
	    then:

		plegado == [mrhaki: 'email@host.com', hubert: 'other@host.com']
	

	}

        def "Prueba de función equivalente a zip"() {

         given: "Dos listas "
	 
         def doslistas=[[1,2],['a','b','c']]

         when:

         def zip = doslistas.transpose() 
          
         then:

	  zip == [[1,'a'],[2,'b']]
        
         

}


     def "Composicion de closures"() {
         given:"Dos clausuaras"
          
          

          def composicion = {f,g,x -> f(g(x))}

	  def f = {a->a+10}
          def g = {a ->a*10}

          when : "Se componen las clasuras"
          def fg = composicion.curry(f,g)
          def gf = composicion.curry(g,f)

          then:
           
          fg(2) == 30
          gf(2) == 120
    

 	}
	


}
	




