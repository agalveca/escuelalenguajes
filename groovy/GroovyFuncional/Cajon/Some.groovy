import groovy.transform.Canonical



@Canonical

class Option<A> {
    
    A referencia

	
    static Option<Object> None = new  Option(null)



    
    static def none() { None}

    // f: A ->  B 
    // resultado Option[B]
	
	def map ( Closure f ) {   

		if ( this == None) None
		else
		new Some(f(referencia).referencia)

	}
    
    // f : A -> B
    // resultado B
	def getOrElse ( Closure f ) {  
		if ( this == None ) f(referencia) 
		else this.referencia
	}
    
    // f A -> Option(B)
    // resultado Option(B)
	def flatMap (Closure f) {
		if ( this == None) None
		else
		f(referencia)
	}

}




class Some<A> extends Option<A>{

	def Some(A a) {
		super(a)
	}


}

