import spock.lang.Specification


@Newify(Some) // en los then no funciona en el lado derecho
 class MapasSomeSpec extends Specification {

    def None = Option.None
    def pets
    def mascotas

    def setup() {
    	pets = [joe:'cat', mary:'turtle', bill:'canary']
    }


	def "Si se invoca una clave que no existe el resultado es null"() {

		given : "Dado un map pets cuando invoca una clave inexistente"
		
		
		expect: "El resultado es null"

		pets.juan == null
		pets['pedro'] == null

	}

	def "Si se construye un objeto Option con los valores del mapa funcionan como se espera" () {

		given : "Dado un map pets cuando invoca una clave existente"
		
		def some = Some(pets.joe)

		def esperado = Some('cat')

		expect : "Una opción con el contenido de la clave"

		some == esperado


	}

	def "Si se construye un objeto Option con una clave inexistente devuelve None" () {

		given : "Dado un map pets cuando invoca una clave existente"
		
		def some = Some(pets.juan)

		

		expect : "Una opción con el contenido de la clave"

		some == None


	}

	def "Los métodos getOrElse y flatMap funcionan correctamente" () {

		given : "Dado un map pets cuando invocan claves existentes o no  u"
		
		def algo = Some(pets.joe)
		def nada = Some(pets.juan)

		

		expect : "Una opción con el contenido de la clave"

		algo.getOrElse { contenido -> contenido +'s'} == 'cat'
		nada.getOrElse { contenido-> contenido+'s'} == 'nulls'
		nada.getOrElse { "Ha habido un error"} == "Ha habido un error"


        algo.flatMap { contenido -> Some(contenido+'s')} == new Some("cats")
		nada.flatMap { contenido -> Some(contenido+'s')} == None



	}

	def "Cuando hay clave pero no hay contenido "() {

	given : "Una clave sin contenido, pero existente"
	mascotas = [joe:'cat', mary:'turtle', bill:'canary',napoleon:'']
	def algoconnada = Some(mascotas.napoleon)
	
	expect: "El resultado no es None"

	 algoconnada != None
	 algoconnada == new Some('')
	 algoconnada != new Some(' ')



}

	


	


}