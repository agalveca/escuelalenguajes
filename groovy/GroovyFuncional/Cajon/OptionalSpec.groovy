import spock.lang.Specification

class OptionalSpec extends Specification {
void 'orNUll returns the reference'() {
given: 'a known reference'
Object expectedReference = new Object()
 
expect: 'orNull returns the reference'
new Optional<Object>(expectedReference).orNull() == expectedReference
}
 
void 'orNUll returns null'() {
expect: 'orNull returns null if there is no reference'
new Optional<Object>().orNull() == null
}
 
void 'get returns the reference'() {
given: 'a known reference'
Object expectedReference = new Object()
 
expect: 'orNull returns the reference'
new Optional<Object>(expectedReference).get() == expectedReference
}
 
void 'get throws NoSuchElementException if there is no reference'() {
when: 'get is called'
new Optional<Object>().get()
 
then: 'orNull returns the reference'
thrown(NoSuchElementException)
}
 
void 'isPresent is false when there is no reference'() {
expect: 'An instance of optional with no reference returns false for isPresent'
!new Optional().isPresent()
}
 
void 'isPresent is true when there is a reference'() {
expect: 'An instance of optional with no reference returns false for isPresent'
new Optional(new Object()).isPresent()
}
 
void 'Delegates to the reference'() {
given: 'An instance of optional with with a list'
Optional<Integer> optionalInteger = new Optional(new Integer(1))
 
expect: 'doubleValue() is invoked on the reference when called on the optional instance'
optionalInteger.doubleValue() == 1.0
}
 
void 'Delegating to a missing reference returns false'() {
given: 'An instance of optional with with a list'
Optional<Integer> optionalInteger = new Optional()
 
expect: 'doubleValue() is invoked on the reference when called on the optional instance'
!optionalInteger.doubleValue()
}
 
void 'can use asBoolean to determine lack of presence of the reference'() {
expect: 'An instance of optional with no reference returns false for isPresent'
!new Optional()
}
 
void 'can use asBoolean to determine presence of the reference'() {
expect: 'An instance of optional with no reference returns false for isPresent'
new Optional(new Object())
}
 
void 'orElse returns the reference if it exists'() {
given: 'a known reference'
Object expectedReference = new Object()
 
and: 'another reference'
Object anotherReference = new Object()
 
expect: 'orElse returns the reference'
new Optional<Object>(expectedReference).orElse(anotherReference) == expectedReference
}
 
void 'orElse returns an alternate if the reference does not exist'() {
given: 'another reference'
Object anotherReference = new Object()
 
expect: 'orElse returns the reference'
new Optional<Object>().orElse(anotherReference) == anotherReference
}
 
void 'orElseTrow returns the reference if it exists'() {
given: 'a known reference'
Object expectedReference = new Object()
 
and: 'an exception'
Exception unexpectedException = new Exception()
 
expect: 'orElse returns the reference'
new Optional<Object>(expectedReference).orElseThrow(unexpectedException) == expectedReference
}
 
void 'orElseTrow throws the given exception if the reference does not exist'() {
given: 'an exception'
Exception expectedException = new Exception()
 
when: 'orElse is called'
new Optional<Object>().orElseThrow(expectedException)
 
then: 'the wtfException is thrown'
Exception exception = thrown()
exception == expectedException
 
}
 
void 'ifPresent returns false if there is no reference present'() {
expect: 'false when there is no reference present'
!new Optional<Object>().ifPresent { return true }
}
 
void 'ifPresent invokes the closure and returns the result if there is a reference present'() {
given: 'an expected closure result'
Integer expectedResult = 10
 
when: 'ifPresent is called with a reference'
Integer result = new Optional<Object>(new Object()).ifPresent { return expectedResult }
 
then: 'the actual result is the expected result'
expectedResult == result
}
}
