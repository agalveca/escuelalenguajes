import spock.lang.Specification

class OpcionalSpec extends Specification {


	void 'orNUll devuelve una referencia o null'() {
		given: 'Una función que devuelve un resultado no nulo'

		def f = { a , b -> new Optional(a+b)},
		    g = { a,b   ->  new Optional()},
		    h = {a,b -> a+b}
		 
		//Object expectedReference = new Object()
		 
		expect: 'orNull devuelve la referencia'
		f(2,3).orNull() == 5
		g(2,3).orNull() == null

		f(2,3).orElse { g(2,3)} == 5 // no llega a este

		g(2,3).orElse (f(2,3).get()) == 5
		g(2,3).orElse (h(2,3)) == 5
		g(2,3).orElse (5) == 5
		g(2,3).orElse {h(2,3)}() == 5
		g(2,3).orElse {5}() == 5

		g(2,3).alternativa {f (2,3)} == 5
		
	}

	def 'orElse devuleve el primer valor, si no es null' () {

		
		 
		given: 'Una función que devuelve un resultado no nulo'

		def f = { a , b -> new Optional(a+b)},
		    g = { a,b   ->  new Optional()},
		    h = {a,b -> a+b}
		 
		expect: 'orNull devuelve la referencia'
		

		f(2,3).orElse { g(2,3)} == 5 // no llega a este
		f(2,3).orElse ( g(2,3)) == 5 // no llega a este

		g(2,3).orElse (f(2,3).get()) == 5
		g(2,3).orElse (h(2,3)) == 5
		g(2,3).orElse (5) == 5
		g(2,3).orElse {h(2,3)}() == 5
		g(2,3).orElse {5}() == 5

		g(2,3).alternativa {f (2,3)} == 5

		

	}

	def 'orElse devuleve el segundo  valor pasando un valor no una opcion' () {

		
		 
		given: 'Una función que devuelve un resultado no nulo'

		def f = { a , b -> new Optional(a+b)},
		    g = { a,b   ->  new Optional()},
		    h = {a,b -> a+b}
		 
		expect: 'orNull devuelve la referencia'
		

		f(2,3).orElse { g(2,3)} == 5 // no llega a este
		f(2,3).orElse ( g(2,3)) == 5 // no llega a este

		g(2,3).orElse (f(2,3).get()) == 5
		g(2,3).orElse (h(2,3)) == 5
		g(2,3).orElse (5) == 5
		

		

	}

	def 'orElse devuleve el segundo  valor pasando una clausura a valor no un valor' () {

		
		 
		given: 'Una función que devuelve un resultado no nulo'

		def f = { a , b -> new Optional(a+b)},
		    g = { a,b   ->  new Optional()},
		    h = {a,b -> a+b}
		 
		expect: 'orNull devuelve la referencia'
		

		g(2,3).orElse {h(2,3)}() == 5
		g(2,3).orElse {5}() == 5

		g(2,3).alternativa {f (2,3)} == 5

		

	}

	def "alternativa devuelve el segundo valor pasando una clausura a opcion "() {

		given: 'Una función que devuelve un resultado no nulo'

		def f = { a , b -> new Optional(a+b)},
		    g = { a,b   ->  new Optional()},
		    h = {a,b -> a+b}
		 
		expect: 'orNull devuelve la referencia'
		

		
        f(2,3).alternativa {f (2,3)} == 5 // No llega a ejecutarse
		g(2,3).alternativa {f (2,3)} == 5

		

	}

	

}