import groovy.transform.Canonical

 
@Canonical
class Optional<T> {
T reference
 
	T get() {
	if (isPresent()) return reference
	 
	throw new NoSuchElementException()
	}
 
	Boolean isPresent() {
	return reference != null
	}
 
	Boolean asBoolean() {
	return isPresent()
	}
 
	T orElse(T other) {
	isPresent() ? reference : other
	}
    
    // Por simetria la clausura debe devolver un Optional
	T  alternativa(Closure c) {
	isPresent() ? reference : c().get()
	}
 
	T orElseThrow(Exception e) {
	if (isPresent()) {
	return reference
	}
 
	throw e
	}
 
	T orNull() {
	return reference
	}
 
	def ifPresent(Closure closure) {
	if (isPresent()) {
	return closure(reference)
	}
	}
 
	@Override
	String toString() {
	return "Optional.of(${reference})"
	}
 
	@Override
	def methodMissing(String name, args) {
	reference?.invokeMethod(name, args)
}
}
 

