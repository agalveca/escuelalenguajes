import spock.lang.Specification


@Newify(Some) // en los then no funciona en el lado derecho
 class SomeSpec extends Specification {

    def None = Option.None


	def "Se puede instanciar un objeto Some"() {

		given : "Dado un objeto Option"
		def some = Some("Hola mundo")

		expect: "Es igual a si mismo"

		some == new Some("Hola mundo")

	}

	def "Se puede usar un objeto None"() {

		given : "Dado un objeto Option"
		def some = Some("Hola mundo")

		expect: "No es igual a None"

		some != None
		None == Option.None

	}


	def "Se puede aplicar map a un objeto Some y a un objeto None" () {

		given : "Dado un objeto Option"

		def some = Some(5)
		def cuadrado = Some(25)

		when: "Se le aplica una función"

		def resultado= some.map { el -> Some(el*el)}

		then : 

		resultado == new Some(25)

		None.map { Some(it*it)} == None

		None.map { it*it} != new Some("Hola Mundo") // a este lado no funciona Newifty


	}


	def "Si la función devuelve None map también" () {

		given : "Dado un objeto Option"

		def some = Some(5)

		when: "Se le aplica una función"

		def resultado= some.map { el -> None}

		then : 

		resultado == None
		resultado != new Some("Hola Mundo")

		

	}

	def "getOrElse funciona con Some y con None" () {

		given : "Dado un objeto Option"

		def some = Some(5)
		def cuadrado = Some(25)

		when: "Se le aplica una función"

		def resultado= some.getOrElse { el -> el*el}

		then : 

		resultado == 5

		None.getOrElse { 5*5} == 25

		None.getOrElse { 5*5} != "Hola Mundo" 

		None.getOrElse {"Hola Mundo"} == "Hola Mundo"


	}

	def "flatMap funciona con Some y con None" () {

		given : "Dado un objeto Option"

		def some = Some(5)
		

		when: "Se le aplica una función"

		def resultado= some.flatMap { el -> Some(el*el)}

		then : 

		resultado == new Some(25)

		some.flatMap { el -> None} == None

		None.flatMap { el -> Some(el*el)} == new Some(null) //equivale a None

		


	}



	


}