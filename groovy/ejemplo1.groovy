#!/usr/bin/env groovy

class Cliente {
    // properties
    Integer id
    String nombre
    Date dob

    // sample code
    static void main(args) {
        def cliente = new Cliente(id:1, nombre:" Franciso, Papa de Roma", dob:new Date())
        println("Hola ${cliente.nombre}")
    }
}