import spock.lang.*

class Ejercicios1 extends spock.lang.Specification {
    
    def "Ejecutar las clausuras de un array"() {

    	given: "Una lista que contendrá closures"

    	def lista =[]

    	def c = 10

    	when: "Añadimos dos clausuras" 
        

        lista.push ({ a,b -> "Hola Mundo" })
        lista.push ({ a,b -> a + b + c})
        
        then: "Ejecutamos todas las clausuras"

        lista*.call(2,3) == ["Hola Mundo", 5]
    }

	
	
	
}