// groovy ejemplo1b.groovy
class Customer {
    // properties
    Integer id
    String nombre
    Date dob

    // sample code
    static void main(args) {
        def cliente = new Customer(id:1, nombre:" ¿Cómo estás?", dob:new Date())
        println("Hola ${cliente.nombre}")
    }
}
