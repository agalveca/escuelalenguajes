import spock.lang.*

class HelloSpock extends spock.lang.Specification {
    def "length of Spock's and his friends' names"() {
        expect:
        name.size() == length

        where:
        name     | length
        "Spock"  | 5
        "Kirk"   | 4
        "Scotty" | 6
    }

	def "test sencillo"() {
		given:
		println "Principio del test"
			def a=3
			def b=5
			
		when:
			def c=a+b
		then:
			8==c
			println "Fin del test"
	}
	
	
}