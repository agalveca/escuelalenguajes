(ns clojureschool.core-test
  (:use clojure.test
        clojureschool.core
		clojureschool.leccion1))



(deftest un-test
	(testing "Funciones diversas definidas en leccion1"
 
	
    (is (= "Hola, Mundo! Cruel" (holamundo)))
	(is (= 5 (+ 2 3)))
	(is (= 5 (average [4 6 5])))
	(is (= 5.0 (hypot 3 4)))
    (is (= '(1 2 4) (accesovector)))
    (is (= '(1 2 4) (otro-acceso-vector)))
    ))
