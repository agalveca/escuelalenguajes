(ns clojureschool.midje-leccion1_test
  (:use midje.sweet clojure.string clojureschool.leccion1)               ;; <<==
  )

(fact "Operaciones básicas"
      (holamundo)          => "Hola, Mundo! Cruel"
      (+ 2 3)              => 5
      (average [4 6 5])    => 5
      (hypot 3 4)          => 5.0
      (accesovector)       => '(1 2 4)
      (otro-acceso-vector) => '(1 2 4)

      )

(fact "`split` splits strings on regular expressions and returns a vector"
  (split "a/b/c" #"/") => ["a" "b" "c"]
  (split "" #"irrelevant") => [""]
  (split "no regexp matches" #"a+\s+[ab]") => ["no regexp matches"])



(facts "about `split`"
  (split "a/b/c" #"/") => ["a" "b" "c"]
  (split "" #"irrelvant") => [""]
  (split "no regexp matches" #"a+\s+[ab]") => ["no regexp matches"])
