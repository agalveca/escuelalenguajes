(ns clojureschool.leccion1)

(defn holamundo[] (str "Hola, Mundo!" " " "Cruel"))
(defn average [numbers] (/ (apply + numbers) (count numbers))) ; libro de oreilly
(defn hypot [x y] (let [	x2 (* x x)  y2 (* y y)] (Math/sqrt (+ x2 y2))))
(defn accesovector []
	(let [vec [1 2 3 4]] 
	(list (first vec) (second vec) (last   vec))  ))
	
(defn otro-acceso-vector []
		(let [vec [1 2 3 4]] 
		(list (nth vec 0) (vec 1 ) (.get   vec 3))  ))