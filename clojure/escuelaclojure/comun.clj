
(ns comun)

(defn ejecutar_esta [funciones indice] ((nth funciones indice)) )


(defn ejecutar_ultima [funciones] (ejecutar_esta  funciones (-  (count funciones) 1) ))

(defn ejecuta_funcion [f] (f))

(defn ejecutar_todas [funciones] (map ejecuta_funcion funciones))

(defn ejecutar_estas [funciones indices] (map #( ejecutar_esta funciones %  ) indices ))
