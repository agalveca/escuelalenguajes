(ns leccion1 (:use comun))


;  hay que distinguir valor
;  de la función que devuelve ese valor

; en lighttable Cmd+SHift+Enter para compilar  y ejecutar el archivo completo

( def funciones [


                 ; 0

                   #(type 3)

                 ; 1

                   #( Long/MAX_VALUE)

                 ; 2

                 #(inc (bigint Long/MAX_VALUE))

                 ; 3

                 #(type 5N)

                 ; 4

                 #(type (int 0))

                 ; 5

                  #(type (short 0))

                 ; 6

                  #(type (byte 0))

                 ; 7
                 #(type 1.23)

                 ; 8
                 #(type ( float 1.23))

                 ; 9
                 #(type 1/3)


                 ])




(ejecutar_esta  funciones 4 )

(ejecutar_estas funciones  [0 1 2 3 4 5 6 7 8 9] )

(ejecutar_todas funciones)

(ejecutar_ultima funciones)

