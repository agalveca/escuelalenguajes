(ns leccion2 (:use comun))


;  hay que distinguir valor
;  de la función que devuelve ese valor

; en lighttable Cmd+SHift+Enter para compilar  y ejecutar el archivo completo

( def funciones [


                 ; 0

                #(+ 1 2)

                 #(+ 1 2.0)

                 #(= 3 3.0)

                 #(== 3 3.0)

                 #(- 3 1)


                 #(* 1.5 3)

                 ;6

                 #(/ 1 2)

                 #(+ 1 2 3)

                 #(* 2 3 1/5)

                 #(- 5 1 1 1)

                 #(/ 24 2 3)

                 ;11

                 #(+ 2)

                 #(- 2)

                 #(* 4)

                 #(/ 4)

                 #(+)

                 ;16

                 #(*)

                 #(<= 1 2 3)

                 #(<= 1 3 2)

                 #(<= 1 1 2)

                 #(< 1 1 2)
                 ;21

                 #(> 3 2 1)

                 #(> 1 2 3)

                 #(inc 5)

                 #(dec 5)

                 #(= 2 2 2)

                 ;26

                 #(= 2 2 3)


                 ])




(ejecutar_esta  funciones 4 )

(ejecutar_estas funciones  [0 1 2 3 4 5 6 7 8 9] )

(ejecutar_todas funciones)

(ejecutar_ultima funciones)
