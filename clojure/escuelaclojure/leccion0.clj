(ns leccion0 (:use comun) )

(defn saludo [] "Hola mundo cruel")



;  hay que distinguir valor
;  de la función que devuelve ese valor

; en lighttable Cmd+SHift+Enter para compilar  y ejecutar el archivo completo

; las funciones que se almacenan en el vector funciones deben ser funciones sin parametros
; para que se puedan ejecutar con las funciones definidas en comun

( def funciones [
                  ; 0

                  (fn []
                    "Función anónima -> String"
                    (str "Hola " "Mundo"))

                 ; 1

                  #( str "Adios " "Mundo")

                 ; 2

                 saludo

                 ; 3
                 ; función definida dentro de otra función

                 (fn []

                   " Definición de una función que cuando se ejecuta
                     invoca una función  de dos parámetros"

                   (let [
                         sumar (fn [a b] (+ a b) )
                         ]
                      (str (sumar 2 4))

                   ))

                 ; 4
                 ; no se permite # anidadas

                  #(
                    let [ prod (fn [a b] (* a b) )]
                      (str (prod 2 4))

                   )



                 ])




(ejecutar_esta  funciones 2 )

(ejecutar_ultima funciones)

(ejecutar_estas funciones  [0 1 2 3 4 ])



(ejecutar_todas funciones)




