(ns leccion3 (:use comun))


;  hay que distinguir valor
;  de la función que devuelve ese valor

; en lighttable Cmd+SHift+Enter para compilar  y ejecutar el archivo completo

( def funciones [


                 ; 0

                 #(type "gato")

                 #(str "cat")

                 #(str 'cat)

                 #(str 1)

                 #(str true)

                 #(str '(1 2 3))

                 #(str nil)

                 ;7

                 #(str "meow " 3 " times")

                 #(re-find #"cat" "mystic cat mouse")

                 #(re-find #"cat" "only dogs here")

                 #(re-matches #"(.+):(.+)" "mouse:treat")

                 #(rest (re-matches #"(.+):(.+)" "mouse:treat"))





                 ])




(ejecutar_esta  funciones 4 )

(ejecutar_estas funciones  [0 1 2 3 4 5 6 7 8 9] )

(ejecutar_todas funciones)

(ejecutar_ultima funciones)
