#!/bin/jsc

if (arguments.length != 2) {
    print('uso:\n $ jsc sumar.js n1 n2');
    quit();
}

load('defsuma.js');

var a = parseInt(arguments[0]);
var b = parseInt(arguments[1]);
print(suma(a,b));
