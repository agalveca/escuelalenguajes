var http = require("http"); 
var url = require("url");

function start(route) {

	function onRequest(request, response) {

	var pathname = url.parse(request.url).pathname; 

		console.log("Petición para" + pathname + " recibida.");
	    route(pathname);
	    response.writeHead(200, {"Content-Type": "text/plain"});
	    response.write("Hola mundo");
	    response.end();
	}
	
	http.createServer(onRequest).listen(8888);
	console.log("Servidor ha arrancado.");
}


exports.start = start;