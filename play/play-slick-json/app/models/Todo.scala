package models

import play.api._
import play.api.Play.current
import play.api.libs.json._
import play.api.db.slick.Config.driver.simple._


case class Todo(id: Option[Long] = None, content: String)

class Todos(tag:Tag) extends Table[Todo](tag,"TODO") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def content = column[String]("content", O.DBType("TEXT"))
  def * = (id.? , content) <>(Todo.tupled, Todo.unapply _)




}

// val myInsert = employees.map(e => (e.empName, e.empType)) returning employees.map(_.empId)

private[models] trait OAD  {

  val Todos    = TableQuery[Todos]
  val buscarConId = Todos.findBy(_.id)
}



object Todos extends OAD {


  def create(todo: Todo)(implicit s:Session) = {
    (Todos returning Todos.map(_.id)).insert(todo)
  }

  def update(id: Long, todo: Todo)(implicit s:Session) = {
    val todoToUpdate: Todo = todo.copy(Some(id))
    Todos.where(_.id === id).update(todoToUpdate)
  }

  def findById(id: Long)(implicit s:Session): Option[Todo] = {
    buscarConId(id).firstOption
  }

  def all()(implicit s:Session): List[Todo] = {
    Todos.sortBy(_.id).list
  }

  def count(implicit s:Session): Int = {
    Query(Todos.length).first
  }

  def delete(id: Long)(implicit s:Session) = {
    Todos.where(_.id === id).delete
  }

}