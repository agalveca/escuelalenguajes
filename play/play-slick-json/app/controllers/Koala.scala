package controllers

import play.api._
import play.api.mvc._

import play.api.Play._
import play.api.data._
import play.api.Play.current
import play.api.libs.json._

import play.api.libs.functional.syntax._



import views._

case class Eucalipto(col:Int,row:Int)

object Eucalipto {

  implicit val fmt = Json.format[Eucalipto]
}

case class Koala(nombre:String,casa:Eucalipto)

object Koala {
  implicit val fmt= Json.format[Koala]
}


object KoalaController extends Controller{

  def miKoala = Action {


    val koala= Koala("kaylee",Eucalipto(10,23))
    println(Json.prettyPrint(Json.toJson(koala)))

    Ok(Json.toJson(koala))
  }


  implicit val rds = (
    (__ \ 'name).read[String] and
    (__ \ 'age).read[Long]
    ) tupled

  def sayHello = Action(parse.json) { request =>
    request.body.validate[(String, Long)].map{
      case (name, age) => Ok("Hello " + name + ", you're "+age)
    }.recoverTotal{
      e => BadRequest("Detected error:"+ JsError.toFlatJson(e))
    }
  }
}
