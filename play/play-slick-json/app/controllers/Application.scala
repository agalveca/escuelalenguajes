package controllers

import play.api._
import play.api.mvc._


import play.api.data._
import play.api.data.Forms._
import play.api.db.slick._
import play.api.Play.current

import play.api.db.slick.Config.driver.simple._

import views._
import models._

object Application extends Controller {


  val Home = Redirect(routes.Application.list(0, 2, ""))


  val computerForm = Form(
    mapping(
      "id" -> optional(longNumber),
      "name" -> nonEmptyText,
      "introduced" -> optional(date("yyyy-MM-dd")),
      "discontinued" -> optional(date("yyyy-MM-dd")),
      "company" -> optional(longNumber)
    )(Computer.apply)(Computer.unapply)
  )


  def index = Action { Home }


  def list(page: Int, orderBy: Int, filter: String) = DBAction { implicit rs =>
    Ok(html.list(
      Computers.list(page = page, orderBy = orderBy, filter = ("%"+filter+"%")),
      orderBy, filter
    ))
  }


  def edit(id: Long) = DBAction { implicit rs =>
    Computers.findById(id).map { computer =>
      Ok(html.editForm(id, computerForm.fill(computer), Companies.options))
    }.getOrElse(NotFound)
  }


  def update(id: Long) = DBAction { implicit rs =>
    computerForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.editForm(id, formWithErrors, Companies.options)),
      computer => {
        Computers.update(id, computer)
        Home.flashing("success" -> "Computer %s has been updated".format(computer.name))
      }
    )
  }


  def create = DBAction { implicit rs =>
    Ok(html.createForm(computerForm, Companies.options))
  }


  def save = DBAction { implicit rs =>
    computerForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.createForm(formWithErrors, Companies.options)),
      computer => {
        Computers.insert(computer)
        Home.flashing("success" -> "Computer %s has been created".format(computer.name))
      }
    )
  }


  def delete(id: Long) = DBAction { implicit rs =>
    Computers.delete(id)
    Home.flashing("success" -> "Computer has been deleted")
  }

  def indexTodo = DBAction { implicit rs =>
    Ok(html.todoForm("Todos", Todos.all(), TodoController.todoForm))
  }


}