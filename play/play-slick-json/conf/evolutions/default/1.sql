# --- Created by Slick DDL
# To stop Slick DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table COMPANY (id  INT PRIMARY KEY AUTO_INCREMENT,name varchar(100) NOT NULL);
create table COMPUTER (id INT PRIMARY KEY AUTO_INCREMENT,name varchar(100) NOT NULL,introduced mediumtext  ,discontinued mediumtext  ,companyId INT);

# --- !Downs

drop table "COMPANY";
drop table "COMPUTER";

