
 require 'sinatra'
 require 'sinatra/activerecord'
 require './modelos/song.rb'


 # Esto es propio de Sinatra/activeRecord
 DB_CONFIG = YAML::load(File.open('config/database.yml'))

 set :database, "mysql2://#{DB_CONFIG['username']}:#{DB_CONFIG['password']}@#{DB_CONFIG['host']}:#{DB_CONFIG['port']}/#{DB_CONFIG['database']}"



get '/songs' do
  @songs = Song.all
  slim :songs
end

 get '/songs/new' do
   @song = Song.new
   slim :new_song
 end

get '/songs/:id' do
  @song = Song.find(params[:id])
  slim :show_song
end

get '/songs/:id/edit' do
  @song = Song.find(params[:id])
  slim :edit_song
end

post '/songs' do
  song = Song.new(params[:song])
  if song.save
  redirect to("/songs/#{song.id}")
  end
end

put '/songs/:id' do
  song = Song.find(params[:id])
  if song.update(params[:song])
  redirect to("/songs/#{song.id}")
  end
end

delete '/songs/:id' do
   Song.find(params[:id]).destroy
   redirect to('/songs')
end
