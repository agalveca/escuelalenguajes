# sinatra_mysql1.rb
require 'sinatra'
require 'sinatra/activerecord'
#require 'yaml'
require './modelos/modelos.rb'
#require 'mysql2'

# Esto es propio de Sinatra/activeRecord
DB_CONFIG = YAML::load(File.open('config/database.yml'))

set :database, "mysql2://#{DB_CONFIG['username']}:#{DB_CONFIG['password']}@#{DB_CONFIG['host']}:#{DB_CONFIG['port']}/#{DB_CONFIG['database']}"

get '/' do
  @items = Averia.all
  "El número de averías #{Averia.count}"
end