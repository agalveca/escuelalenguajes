# Aplicacion Scalatra de ejemplo #

## Build & Run ##

```sh
$ cd Aplicacion_Scalatra_de_ejemplo
$ ./sbt
> container:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.
