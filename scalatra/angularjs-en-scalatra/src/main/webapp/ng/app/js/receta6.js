function MyCtrl($scope) {
    $scope.value = "some value";

    $scope.valor = 1;

    $scope.incrementValue = function(incremento) {
        $scope.valor += incremento;
    };

    $scope.getIncrementedValue = function() {
        return $scope.valor + 1;
    };

    $scope.name = "";

    $scope.$watch("name", function(newValue, oldValue) {
        if (newValue.length > 0) {
            $scope.greeting = "Greetings " + newValue;
        }
    });
}