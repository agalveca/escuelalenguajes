/**
 * Created by agc on 29/01/14.
 */


var app = angular.module("MyApp", []);

app.directive("myWidget", function() {
    var linkFunction = function(scope, element, attributes) {
        scope.text = attributes["myWidget"];
    };

    return {
        restrict: "A",
        template: "<p>El texto en el atributo era {{text}}</p>",
        link: linkFunction,
        scope: {}
    };
});

app.directive("myWidgetBi", function() {
    return {
        restrict: "E",
        template: "<p>El texto en el atributo era {{text}}</p>",
        scope: {
            text: "@text"
        }
    };
});

app.directive("myWidgetExpr", function() {
    var linkFunction = function(scope, element, attributes) {
        scope.text = scope.fn({ count: 5 });
    };

    return {
        restrict: "E",
        template: "<p>El valor es{{text}}</p>",
        link: linkFunction,
        scope: {
            fn: "&fn"
        }
    };
});