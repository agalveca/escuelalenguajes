/**
 * Created by agc on 01/02/14.
 */

var app = angular.module("MyApp", []);

app.filter("reverse", function() {
    return function(input) {
        var result = "";
        input = input || "";
        for (var i=0; i<input.length; i++) {
            result = input.charAt(i) + result;
        }
        return result;
    };
});

app.filter("exclude", function() {
    return function(input, exclude) {
        var result = [];
        for (var i=0; i<input.length; i++) {
            if (input[i] !== exclude) {
                result.push(input[i]);
            }
        }

        return result;
    };
});

app.filter('checkmark', function() {
    return function(input) {
        return input ? '\u2713' : '\u2718';
    };
});
