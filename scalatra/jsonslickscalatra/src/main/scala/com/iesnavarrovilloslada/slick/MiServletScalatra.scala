package com.iesnavarrovilloslada.slick

import org.scalatra._
import net.liftweb.json._
import net.liftweb.json.Serialization.{ read, write }
import scalate.ScalateSupport

//import scala.slick.driver.H2Driver.simple._
import scala.slick.driver.MySQLDriver.simple._


case class MiServletScalatra(db: Database) extends ScalatraServlet with SlickRoutes

trait SlickRoutes  extends JsonslickscalatraStack {

  implicit val formats = Serialization.formats(NoTypeHints);

  val db: Database

  val suppliers = TableQuery[Suppliers]
  val coffees=TableQuery[Coffees]

  class Suppliers(tag:Tag) extends Table[(Int, String, String, String, String, String)](tag,"SUPPLIERS") {
    def id = column[Int]("SUP_ID", O.PrimaryKey) // This is the primary key column
    def name = column[String]("SUP_NAME")
    def street = column[String]("STREET")
    def city = column[String]("CITY")
    def state = column[String]("STATE")
    def zip = column[String]("ZIP")

    def * = (id, name,street ,city,state ,zip)
  }


  class Coffees(tag:Tag) extends Table[(String, Int, Double, Int, Int)](tag,"COFFEES") {
    def name = column[String]("COF_NAME", O.PrimaryKey)
    def supID = column[Int]("SUP_ID")
    def price = column[Double]("PRICE")
    def sales = column[Int]("SALES")
    def total = column[Int]("TOTAL")
    def * = (name,supID,price,sales,total)


    def supplier = foreignKey("SUP_FK", supID, suppliers)(_.id)
  }


  get("/db/create-tables") {
    db withSession {
      implicit session=>

      (suppliers.ddl ++ coffees.ddl).create
    }
  }

  get("/db/load-data") {
    db withSession {
      // Insert some suppliers
      implicit session=>
      suppliers.insert(101, "Acme, Inc.", "99 Market Street", "Groundsville", "CA", "95199")
      suppliers.insert(49, "Superior Coffee", "1 Party Place", "Mendocino", "CA", "95460")
      suppliers.insert(150, "The High Ground", "100 Coffee Lane", "Meadows", "CA", "93966")

      // Insert some coffees (using JDBC's batch insert feature, if supported by the DB)
      coffees.insertAll(
        ("Colombian", 101, 7.99, 0, 0),
        ("French_Roast", 49, 8.99, 0, 0),
        ("Espresso", 150, 9.99, 0, 0),
        ("Colombian_Decaf", 101, 8.99, 0, 0),
        ("French_Roast_Decaf", 49, 9.99, 0, 0)
      )
    }
  }

  get("/db/drop-tables") {
    db withSession {
      implicit session=>
      (suppliers.ddl ++ coffees.ddl).drop
    }
  }

  get("/coffees") {
    db withSession {
      implicit session=>
      val q3 = for {
        c <- coffees
        s <- c.supplier
      } yield (c.name.asColumnOf[String], s.name.asColumnOf[String])

      contentType = "text/html"
      q3.list.map { case (s1, s2) => "  " + s1 + " supplied by " + s2 } mkString "<br />"
    }
  }

  get("/cafes") {

    contentType = "application/json";

    db withSession {
      implicit session=>
        val q3 = for {
          c <- coffees
          s <- c.supplier
        } yield (c.name.asColumnOf[String], s.name.asColumnOf[String])

        contentType = "text/html"
        val result= q3.list
        Ok(write(result));
    }

  }


}











