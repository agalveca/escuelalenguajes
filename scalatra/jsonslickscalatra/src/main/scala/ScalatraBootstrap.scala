import com.iesnavarrovilloslada.slick._
import org.scalatra._
import javax.servlet.ServletContext
import com.mchange.v2.c3p0.ComboPooledDataSource
import org.slf4j.LoggerFactory
import scala.slick.driver.H2Driver.simple._





class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {

    val db = Database.forDataSource(cpds)
    //Database.forURL("jdbc:mysql://localhost/test?user=root", driver = "com.mysql.jdbc.Driver") withSession{

    context.mount(MiServletScalatra(db), "/*")

  }


  val logger = LoggerFactory.getLogger(getClass)

  val cpds = new ComboPooledDataSource
  logger.info("Created c3p0 connection pool")

  private def closeDbConnection() {
    logger.info("Closing c3po connection pool")
    cpds.close
  }

  override def destroy(context: ServletContext) {
    super.destroy(context)
    closeDbConnection
  }
}
