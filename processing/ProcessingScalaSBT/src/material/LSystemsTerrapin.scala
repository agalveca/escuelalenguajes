package lsystems
import processing.core._
import PConstants._
import PApplet._
import terrapin._


case class LSystem(axiom: String, angle: Double, len:Int, sf: Double ,t:Terrapin)(rules: PartialFunction[Char, String]) {
    var currVal = axiom
    var currGen = 0
    var xPos:Int= _
    var yPos:Int= _
    
    def evolve() {
        currGen += 1
        currVal = currVal.map { c =>
            if (rules.isDefinedAt(c)) rules(c) else c
        }.mkString.replaceAll("""\|""" , currGen.toString)
    }
    
    def savePos(){
      xPos=t.x
      yPos=t.y
    }
    
    def restorePos(){
      t.drawing=false;
      t.setLocation(xPos,yPos)
      t.drawing=true;
    }
    
    def draw() {
        def isDigit(c: Char) = Character.isDigit(c)
        val genNum = new StringBuilder
        def maybeDrawBar() {
            if (genNum.size != 0) {
                val n = genNum.toString.toInt
                genNum.clear()
                t.forward(len * pow(sf.toFloat, n).toInt)
            }
        }
        currVal.foreach { c => 
            if (!isDigit(c)) {
                maybeDrawBar()
            }
            
            c match {
                case 'F' => t.forward(len)
                case 'f' => t.forward(len)
                case 'G' => t.drawing=false; t.forward(len); t.drawing=true
                case '[' => savePos()
                case ']' => restorePos()
                case '+' => t.right(angle.toInt)
                case '-' => t.left(angle.toInt)
                case n if isDigit(n) => genNum.append(n)
                case _ => 
            }
        }
        maybeDrawBar()
    }
}
 

 


 

 





class Main extends PApplet {
	
	val t = new Terrapin(this)
	
	t.setPenColor(255, 0, 0)
	
	
	val sierp_wp6 = LSystem("F", 60, 4,0.6,t) {

	    case 'F' => "f+F+f"
	    case 'f' => "F-f-F"
	}
	
	val drawing=sierp_wp6
  
  override def setup {
    size(1200, 1200)
    colorMode(HSB, 200)
    frameRate(999)
    background(10,80,200)
    noLoop
  }

  override def draw {
	
	strokeWeight(1)
	t.drawing=false
	t.setLocation(width/2,2*height/3)
	t.left(60)
	t.drawing=true

	val gens = 7
	for (i<-0 to gens)
	 {
		
	    drawing.evolve()
	}
	drawing.draw()
   }
}

object Main {
  def main(args:Array[String]) {
    PApplet.main(Array("lsystems.Main"))
  }
}
