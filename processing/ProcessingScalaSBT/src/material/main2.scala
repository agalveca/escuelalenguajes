package kuhn
import terrapin._

import processing.core._
import PConstants._
import PApplet._
import math._

class Principal extends PApplet {
 
	
	
	override def setup {
	    size(800, 800)
	    
	    background(0)
	    noLoop
	  }
	
	override def draw {
		val t:Terrapin = new Terrapin(this)

		// set the Terrapin's pen color to an r,g,b value
		t.setPenColor(255, 0, 0)

		// move the Terrapin to draw a square
		t.forward(100)
		t.right(90)
		t.forward(100)
		t.right(90)
		t.forward(100)
		t.right(90)
		t.forward(100)
		
	    
	}
  
}

object Principal {
  def main(args:Array[String]) {
    PApplet.main(Array("kuhn.Principal"))
  }
}









