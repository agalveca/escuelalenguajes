package kuhn
import terrapin._

import processing.core._
import PConstants._
import PApplet._
import math._

class Spiro extends PApplet {
 
	val t:Terrapin = new Terrapin(this)
		t.setPenColor(255, 0, 0)
	
	override def setup {
	    size(800, 800)
	    
	    background(0)
	    
	  }
	
	override def draw {
		
		t.forward(100);
		  t.right(85);

		
		
	    
	}
  
}

object Spiro {
  def main(args:Array[String]) {
    PApplet.main(Array("kuhn.Spiro"))
  }
}