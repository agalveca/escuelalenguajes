package simple

import processing.core._
import processing.core.PConstants._



object EjemplosSimples {

   class AppletProcessing extends PApplet {

   		override def setup() {
		    size(400, 400)	    
		    fill(0, 102, 153, 204)
		    
	  }
	  		    
	  }

   val ejemplo1 = new AppletProcessing {

   
	   override def draw() {

		rectMode(CENTER)
		rect(100,100,20,100)
		ellipse(100,70,60,60)
		ellipse(81,70,16,32) 
		ellipse(119,70,16,32) 
		line(90,150,80,160)
		line(110,150,120,160)


      }

   }
   val ejemplo2 = new PApplet {
	override def setup() {
	    size(200, 200)
	    noStroke( )
	    fill(0, 102, 153, 204)
	  }

	  override def draw() {
	    background(255)
	    rect(width-mouseX, height-mouseY, 50, 50)
	    rect(mouseX, mouseY, 50, 50)
	  }
}

   val definiciones:Array[PApplet]=Array( ejemplo1)
}

object VerEjemplosSimples extends processing.core.PApplet {

  

	def main(args: Array[String]) {
	    val frame = new javax.swing.JFrame("Ejemplo s")
	    val activo:PApplet=EjemplosSimples.definiciones(0)
	    frame.getContentPane().add(activo)
	    frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE)
	    activo.init

	    frame.pack
	    frame.setVisible(true)
	  }

  
}

