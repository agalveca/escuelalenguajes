package basico



import processing.core._
import PConstants._
import PApplet._

class Ejemplo2 extends PApplet {
  
  override def setup {
     size(1000,700)
     background(255)
  }

  

  override def draw {
   stroke(random(255),random(255), random(255))
   line(random(width),random(height), random(width),random(height))
  
  }
  
}

object Ejemplo2 {
  def main(args:Array[String]) {
    PApplet.main(Array("basico.Ejemplo2"))
  }
}