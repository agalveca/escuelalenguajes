package basico

import processing.core._
import PConstants._
import PApplet._

object Ejemplo1 extends PApplet {

  override def setup() {
    size(200, 200)
    noStroke( )
    fill(0, 102, 153, 204)
  }

  override def draw() {
    background(255)
    rect(width-mouseX, height-mouseY, 50, 50)
    rect(mouseX, mouseY, 50, 50)
  }

	def main(args: Array[String]) {
	    val frame = new javax.swing.JFrame("Ejemplo 1")
	    frame.getContentPane().add(basico.Ejemplo1)
	    Ejemplo1.init

	    frame.pack
	    frame.setVisible(true)
	  }

  
}
