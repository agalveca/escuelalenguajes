
// una factoria que devuelve una función
// a processing.setup se le debe pasar una funcion

var configurar= function (p,ancho,alto,fondo,llenado) {

    return function() {
        p.size(ancho, alto);
        p.background(fondo);
        p.fill(llenado);
    };

};

function ejemploRectangulos(p) {

    p.setup= configurar(p,250,100,209,162);

    p.draw = function() {
        var cx=30,cy=20;
        p.rect(cx, cy, 55, 55);
        p.rect(cx+60, cy, 55, 55, 7);             //el mismo radio en todas las esquinas
        p.rect(cx+120, cy, 55, 55, 3, 6, 12, 18); //diferente radio


    };
}

function ejemploElipses(p) {

    p.setup= configurar(p,150,100,209,162)

    p.draw = function() {
        var cx=56,cy=46;
        p.noFill(); // para evitar que la segunda tape a la primera
        p.ellipse(cx, cx, 55, 55);
        p.ellipse(cx, cx, 100, 55);
    };
}

function ejemploArcos(p) {

    p.setup= configurar(p,150,100,209,162)

    p.draw = function() {

        p.arc(50, 55, 100, 50, 0, 3*p.HALF_PI); // trozo de elipse desde 0 a 3 pi medios en sentido horario

    };
}



function ejemploGenerico(p) {

    p.setup= configurar(p,150,100,209,162)

    p.draw = function() {

    };
}




var canvas = document.getElementById("canvas1");
var processingInstance = new Processing(canvas, ejemploRectangulos);
var canvas2 = document.getElementById("canvas2");
var pI = new Processing(canvas2, ejemploElipses);

var canvas3 = document.getElementById("canvas3");
var pA = new Processing(canvas3, ejemploArcos);