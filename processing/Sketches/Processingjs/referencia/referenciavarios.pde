
void setup() {
  size(400, 400);
  background(3,224,187);
  fill(255, 0, 0);
  text("Hello!", 150, 50);

  // Use \n to break text over multiple lines
  text("Hola \nMundo!", 50, 100);

  println("hola mundo web!");
}

void draw() {

fill(238, 255, 0);
  stroke(255);
  ellipse(70, 50, 100, 25);
  stroke(36, 181, 111);
  triangle(150, 115, 250, 115, 200, 30);
  noFill();
  triangle(250, 215, 250, 115, 200, 30);
  strokeWeight(10);
  ellipse(200, 200, 200, 200);

  var strokeColor = color(255, 248, 120);
  var fillColor = color(46, 220, 232);

  stroke(strokeColor);
  fill(fillColor);

  strokeWeight(2);
  ellipse(200, 200, 100, 100);

}