/**
 * Created by agc on 22/03/14.
 */

function Turtle(xin,yin,p){

    //  xin yin posicion inicial
    //  angulo inicial
    //  posicion del lapiz
    this.angle   = -90;
    this.penDown = true;

    this.x=xin;
    this.y=yin;


    this.forward = function (distance) {


        var xtarget = this.x+ p.cos(p.radians(this.angle)) * distance;
        var ytarget = this.y+ p.sin(p.radians(this.angle)) * distance;


        if (this.penDown) p.line(this.x, this.y, xtarget, ytarget);


        this.x = xtarget;
        this.y = ytarget;

    }


    this.back= function (distance) {
        this.forward(-distance);
    }

    this.left=function(turnangle) {
        this.angle -= turnangle;

    }

    this.right= function ( turnangle) {
        this.angle += turnangle;
    }


    this.penUp=function () {
        this.penDown = false;
    }


    this.penDown=function() {
        this.penDown = true;
    }


}









