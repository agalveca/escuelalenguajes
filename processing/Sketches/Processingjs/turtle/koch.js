function fractalKoch(p) {

    var currentSize = 400;

    p.setup= function () {
        p.size(600,600);
        p.background(200);
        p. smooth();
    }

    p.draw = function() {

        var  t = new Turtle(100,400,p);

        t.right(30);

        koch(t,currentSize);
    };

    p.mousePressed=function() {
        currentSize +=100;
        p.background(200);
        p.draw();

    }


    function koch( turtle, x) {
        triline(turtle,x);
        turtle.right(120);
        triline(turtle,x);
        turtle.right(120);
        triline(turtle,x);
        turtleright(120);
    }

    //t es Turtle
    function triline(t, x) {
        if (x<1) t.forward(x);
        else {
            triline(t,x/3);
            t.left(60);
            triline(t, x/3);
            t.right(120);
            triline(t,x/3);
            t.left(60);
            triline(t,x/3);
        }
    }
}

var canvas = document.getElementById("canvas1");
var processingInstance = new Processing(canvas, fractalKoch);






