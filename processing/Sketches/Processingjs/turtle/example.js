function floral(p) {

    var currentSize = 400;
    var t ; // Turtle

    p.setup= function () {
        p.size(600,600);
        p.background(230);
        p. smooth();
        t = new Turtle(300,300,p);
    }


    p.draw = function() {

        mn_eck(t,36,20);
    };

    // t es turtle
    function n_eck(t,  ne,  sz) {

        for (var i=0; i<ne; i++) {
            t.right(360/ne);
            t.forward(sz);
        }
    }

    function mn_eck(t, ne, sz) {
        for (var  i=0; i<ne; i++) {
            t.right(360/ne);
            n_eck(t,ne,sz);
        }
    }



}

var canvas = document.getElementById("canvas2");
var processingInstance2 = new Processing(canvas, floral);