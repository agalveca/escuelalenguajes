class Turtle {
  float x, y; 
  float angle = -90; 
  boolean down = true; 
  
  
  Turtle (float xin, float yin) {
    x = xin;
    y = yin;
  }
  
  void forward (float distance) {
    
    
    float xtarget = x+cos(radians(angle)) * distance;
    float ytarget = y+sin(radians(angle)) * distance;
    
    
    if (down) line(x, y, xtarget, ytarget);
    
    
    x = xtarget;
    y = ytarget;
    
  }
  
   void right (float turnangle) {
    angle += turnangle;
  }

  void left (float turnangle) {
    angle -= turnangle;
  }

  void back (float distance) {
    forward(-distance);
  }

   void penUp() {
    down = false;
  }

   void penDown() {
    down = true;
  }
  
}