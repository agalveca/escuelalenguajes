float currentSize = 400.0;

Turtle t;

void setup() {


  size(600,600);
  background(200);
  noLoop();
  smooth();

  t = new Turtle(100.0,400.0);

  t.right(30.0);

  koch(t,currentSize);
}


void draw() {
}

void koch(Turtle t, float x) {
  triline(t,x); t.right(120);
  triline(t,x); t.right(120);
  triline(t,x); t.right(120);
}

void triline(Turtle t, float x) {
  if (x<1) t.forward(x);
  else {
    triline(t,x/3);
    t.left(60);
    triline(t, x/3);
    t.right(120);
    triline(t,x/3);
    t.left(60);
    triline(t,x/3);
  }
}

void mousePressed() {
  currentSize +=100;
  setup();
}
