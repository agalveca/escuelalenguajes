void setup() {
  size(600,600);
  background(255);
  smooth();
  Turtle t = new Turtle(300,300);
  
  mn_eck(t,36,20);
}

void n_eck(Turtle t, float ne, float sz) {
  for (int i=0; i<ne; i++) {
    t.right(360/ne);
    t.forward(sz);
  }
}

void mn_eck(Turtle t, float ne, float sz) {
  for (int i=0; i<ne; i++) {
    t.right(360/ne);
    n_eck(t,ne,sz);
  }
}
