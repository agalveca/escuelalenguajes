import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class koch extends PApplet {

float currentSize = 400;

public void setup() {
  size(600,600);
  background(255);
  smooth();
  
  Turtle t = new Turtle(100,400);
  
  t.right(30);
  koch(t,currentSize);
}

public void draw() {
}

public void koch(Turtle t, float x) { 
  triline(t,x); t.right(120);
  triline(t,x); t.right(120);
  triline(t,x); t.right(120);
}

public void triline(Turtle t, float x) {
  if (x<1) t.forward(x);
  else {
    triline(t,x/3);
    t.left(60);
    triline(t, x/3);
    t.right(120);
    triline(t,x/3);
    t.left(60);
    triline(t,x/3);
  }
}

public void mousePressed() {
  currentSize +=100;
  setup();
}
class Turtle {
  float x, y; // Current position of the turtle
  float angle = -90; // Current heading of the turtle
  boolean penDown = true; // Is pen down?
  
  // Set up initial position
  Turtle (float xin, float yin) {
    x = xin;
    y = yin;
  }
  
  // Move forward by the specified distance
  public void forward (float distance) {
    
    // Calculate the new position
    float xtarget = x+cos(radians(angle)) * distance;
    float ytarget = y+sin(radians(angle)) * distance;
    
    // If the pen is down, draw a line to the new position
    if (penDown) line(x, y, xtarget, ytarget);
    
    // Update position
    x = xtarget;
    y = ytarget;
    
  }
  
  // Move back by specified distance
  public void back (float distance) {
    forward(-distance);
  }
  
  // Turn left by given angle
  public void left (float turnangle) {
    angle -= turnangle;
  }
  
  // Turn right by given angle
  public void right (float turnangle) {
    angle += turnangle;
  }
  
  // Set the pen to be up
  public void penUp() {
    penDown = false;
  }
  
  // Set the pen to be down
  public void penDown() {
    penDown = true;
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "koch" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
