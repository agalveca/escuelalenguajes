
import processing.core.*

class Flores extends PApplet {

	def amarillo = color(243, 247, 166)
	def verde = color(97, 220, 72)
	def fondo = color(248, 251, 233)
	
void setup() {
  size(400, 400)
  smooth()
  noStroke()
  frameRate(15)
}

void draw() {
  background(fondo)
  
  translate(mouseX, mouseY)
  
  rotate(radians(frameCount + mouseX))
  
  fill(verde) // green
  for (int i = 0; i < 5; i++) {
    ellipse(0, -40, 50, 50)
    rotate(radians(72))
  }
  
  fill(amarillo) 
  ellipse(0, 0, 50, 50)
}


static void main(args) {
   PApplet.main( [ "Flores" ] as String[] )
  }
}