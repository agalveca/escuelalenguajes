package basicos

import processing.core.*
import processing.core.PConstants.*

class Ej1 extends PApplet {

	void setup() {
         size 400, 400
         stroke 255
         background 192, 64, 0
	}

     void draw() {
         line(150, 25, this.mouseX, this.mouseY)
      }

    void mousePressed() {
       background(192, 64, 0)
   }


}

class Ej2 extends PApplet {
	void setup() {
	    size(200, 200)
	    noStroke( )
	    fill(0, 102, 153, 204)
	  }

	  void draw() {
	    background(255)
	    rect(width-mouseX, height-mouseY, 50, 50)
	    rect(mouseX, mouseY, 50, 50)
	  }

}


class Ej3 extends PApplet {

   		void setup() {
		    size(400, 400)	    
		    fill(0, 102, 153, 204)
		    
	  }
	  		       
	   void draw() {

		rectMode(CENTER)
		rect(100,100,20,100)
		ellipse(100,70,60,60)
		ellipse(81,70,16,32) 
		ellipse(119,70,16,32) 
		line(90,150,80,160)
		line(110,150,120,160)


      }

}