package basicos

import processing.core.*


class Ejemplos0  {
 
  static PApplet[] definiciones= [ new Ej1() , new Ej2(), new Ej3()]

  static void  main(args) {

  	    def ejemplo=2
	    def frame = new javax.swing.JFrame("Ejemplo s")
	    PApplet activo = definiciones[ejemplo]
	    frame.getContentPane().add(activo)
	    frame.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE)
	    activo.init()

	    frame.pack()
	    frame.setVisible(true)
	  }
}


