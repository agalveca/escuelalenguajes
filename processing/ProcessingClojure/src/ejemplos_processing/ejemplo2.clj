(ns ejemplos-processing.ejemplo2)
(import [processing.core PApplet])
 
(def angle (atom 0))
 
(def random (java.util.Random.))
 
(def papplet
  (proxy [PApplet] []
	
    (PApplet [] (println "in constructor"))

    (setup []
      (doto this
        (.size 640 360)
        (.background 102)
        (.smooth)
        (.noStroke)
        (.fill 102)))

    (draw []
      (let [_ (swap! angle + 10)
            value (* (Math/cos (Math/toRadians @angle)) 6)
            ]
        (loop [a 0]
          (if (< a 360)
            (let [xoff  (* (Math/cos (Math/toRadians a)) value)
                  yoff  (* (Math/sin (Math/toRadians a)) value)]
                  (doto this
                    (.fill (. random nextInt 255))
                    (.ellipse (+ (. this mouseX) xoff) (+ (. this mouseY) yoff) value value))
                  (recur (+ a 75)))))
        (doto this
          (.fill 255)
          (.ellipse (. this mouseX) (. this mouseY) 2 2))))))
(.init papplet)
 
(def frame (javax.swing.JFrame. "Test XXX"))
 
(doto frame
  (-> (.getContentPane) (.add papplet))
  (.pack)
  (.setVisible true))

(defn -main [& args]
 (frame)	
  (println "Hello world!"))
