(ns ejemplos-processing.ejemplo0)
(import [processing.core PApplet])


(def papplet
  (proxy [PApplet] []
    (PApplet [] (println "in constructor"))
    (setup []
      (doto this
        (.size 400 400)
        (.background 192 64 0)
        (.smooth)
        (.stroke 255)
        (.fill 102)))
    (draw [] (doto this (.line 150 25 (. this mouseX) (. this mouseY) )))))
 
(.init papplet)
 
(def frame (javax.swing.JFrame. "Test de processing"))
 

(doto frame
  (-> (.getContentPane) (.add papplet))
  (.pack)
  (.setVisible true))


