(ns basicos.processingej1
(:import (processing.core PApplet))	
(:require basicos.papplets)	)

;; con require se necesita basico.papplets/definiciones
;; con use no
(def figuras basicos.papplets/definiciones)

(def papplet (nth figuras 1))


(.init papplet)
 
(def frame (javax.swing.JFrame. "Test de processing"))
 

(doto frame
  (-> (.getContentPane) (.add papplet))
  (.pack)
  (.setVisible true))