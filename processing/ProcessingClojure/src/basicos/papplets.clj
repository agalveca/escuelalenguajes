(ns basicos.papplets
	(:import (processing.core PApplet)))
	
	
(def def1

	  (proxy [PApplet] []

	    (setup []
	      (doto this
	        (.size 400 400)
	        (.background 192 64 0)
	        (.smooth)
	        (.stroke 255)
	        ))

	    (draw [] 
		(doto this 
		(.line 150 25 (. this mouseX) (. this mouseY) )))))	
			
(def def2  
  (proxy [PApplet] []    
    (setup []
      (doto this
        (.size 200 200)       
        (.noStroke)
        (.fill 0 102 153 204)))

    (draw [] 
	(doto this 
	(.background 255)
	(.rect (- (. this width) (. this mouseX)) (- (. this height) (. this mouseY)) 50 50)	
	(.rect (. this mouseX) (. this mouseY) 50 50)
	))))
	
(def definiciones [def1 def2])	