(ns basicosquil.quil-ej2
  (:use quil.core))

;; segundo ejemplo
(defn setup[]
	(no-stroke)
	(fill 0 102 153 204))
	
(defn draw[]
	(background 255)
	(line 150 25 (mouse-x) (mouse-y) )
	(rect (- (width) (mouse-x)) (- (height) (mouse-y)) 50 50)
	(rect (mouse-x) (mouse-y) 50 50)
	)
	
	
(defsketch ejemplo                  
	  :title "Cuadrados moviles"  
	  :setup setup                      
	  :draw draw                        
	  :size [200 200])	


	

	    
