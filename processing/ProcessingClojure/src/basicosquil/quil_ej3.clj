(ns basicosquil.quil-ej3
  (:use quil.core))

;; segundo ejemplo
(defn setup[]
	
	(fill 0 102 153 204))
	
(defn draw[]
	(rect-mode :center)
	(rect 100 100 20 100)
	(ellipse 100 70 60 60)
	(ellipse 81 70 16 32) 
	(ellipse 119 70 16 32) 
	(line 90 150 80 160)
	(line 110 150 120 160)
	)
	
	
(defsketch ejemplo                  
	  :title "Muñeco de nieve"  
	  :setup setup                      
	  :draw draw                        
	  :size [400 400])	

	