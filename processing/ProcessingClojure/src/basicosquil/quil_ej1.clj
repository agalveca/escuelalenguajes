(ns basicosquil.quil-ej1
  (:use quil.core))

;; primer ejemplo
(defn setup[]
	(background 192 64 0)
	(smooth)
	(stroke 255))
	
(defn draw[]
	(line 150 25 (mouse-x) (mouse-y) ))
	
	
(defsketch ejemplo                  
	  :title "Oh so many grey circles"  
	  :setup setup                      
	  :draw draw                        
	  :size [400 400])	
	
	

  