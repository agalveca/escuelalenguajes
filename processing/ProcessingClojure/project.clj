(defproject ejemplos-processing "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
 ;; :resource-paths ["lib/core.jar" ]
 

  :dependencies [[org.clojure/clojure "1.4.0"]
                 [quil "1.6.0"]
                 [org.clojars.processing-core/org.processing.core "1.5.1"]
                 [org.clojars.processing-core/org.processing.gluegen-rt "1.5.1"]
                 [org.clojars.processing-core/org.processing.jogl "1.5.1"]
                 [org.clojars.processing-core/org.processing.opengl "1.5.1"]
                 [org.clojars.processing-core/org.processing.itext "1.5.1"]
                 [org.clojars.processing-core/org.processing.pdf "1.5.1"]]
  :aot [quil.applet]
  ;;:main ejemplos-quil.gen-art.cruz-con-circulo
  ;;:main ejemplos-quil.for-the-glory-of-art

  :main basicosquil.quil-texto
 
)
