//
//  funciones1.swift
//  PrimerPaso
//
//  Created by Armando Galve Carbó on 05/06/14.
//  Copyright (c) 2014 es.agc. All rights reserved.
//


func inicializar_funciones1()->Int {
funciones=[paso1,paso2,paso3,paso4,paso5,paso6,paso7,paso8]
    return funciones.count
    
}



func paso1(personName: String) -> String {
    return "Hola de nuevo, " + personName + "!"
}

func paso2(argumento: String) -> String {
    
    let label = "The width is "
    let width = 94
    let widthLabel = label + String(width)
    
    let apples = 3
    let oranges = 5
    let appleSummary = " I have \(apples) apples."
    let fruitSummary = " I have \(apples + oranges) pieces of fruit."
    
    
    
    return (widthLabel + appleSummary + fruitSummary)
    
    
    }

func paso3(argumento:String)->String {
    
    let individualScores = [75, 43, 103, 87, 12]
    var teamScore = 0
    for score in individualScores {
        if score > 50 {
            teamScore += 3
        } else {
            teamScore += 1
        }
    }
    return String(teamScore)
}

func paso4(a:String)->String {
    var optionalName: String? = "John Appleseed"
    
    if(a=="")
    {
        optionalName = nil
        
    }
    
    var greeting = "Hello!"
    
    if let name = optionalName {
        greeting = "Hello, \(name)"
    }
    
    return greeting
}

func paso5(arg:String)->String {
    
    var resultado:String
    
    switch arg {
        case "hola":
        resultado = "Saludo"
        case "adios":
        resultado = "Despedida"
        default:
        resultado = "Vacío"
    }
    
    return resultado
    
    
}

func paso6(arg:String)->String {
    
    let interestingNumbers = [
        "Prime": [2, 3, 5, 7, 11, 13],
        "Fibonacci": [1, 1, 2, 3, 5, 8],
        "Square": [1, 4, 9, 16, 25],
    ]
    var largest = 0
    var tipo:String="desconocido"
    
    for (kind, numbers) in interestingNumbers {
        for number in numbers {
            if number > largest {
                largest = number
                tipo = kind
            }
        }
    }
    return "El número mayor es \(largest) y es de tipo \(tipo)"
    
    
}

func paso7(arg:String)->String {
    var n = 2
    while n < 100 {
        n = n * 2
    }
    
    
    var m = 2
    do {
        m = m * 2
    } while m < 100
    
    return "La variable n vale \(n) la variable m \(m)"
  
}

func paso8(arg:String)->String {
    var firstForLoop = 0
    for i in 0..3 {
        firstForLoop += i
    }
    
    
    var secondForLoop = 0
    for var i = 0; i < 3; ++i {
        secondForLoop += 1
    }
    return "El primer bucle tiene valor \(firstForLoop) y el segundo \(secondForLoop)"
    
}

