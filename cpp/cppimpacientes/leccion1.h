//
//  Leccion1.h
//  C++ParaImpacientes
//
//  Created by Armando Galve Carbó on 08/10/13.
//  Copyright (c) 2013 Armando Galve Carbó. All rights reserved.
//

#ifndef __C__ParaImpacientes__Leccion1__
#define __C__ParaImpacientes__Leccion1__

#include <functional>
#include <iostream>
#include <vector>
using namespace std;

#endif /* defined(__C__ParaImpacientes__Leccion1__) */

namespace leccion1 {


vector<function <void()>> definiciones();
    
void ejecutarejemplo();
void ejemplo1();
void ejemplo2();
void ejemplo3();
void ejemplo4();
void ejemplo5();
void ejemplo6();
void ejemplo7();

}
