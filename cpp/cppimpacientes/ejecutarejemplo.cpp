//
//  ejecutarejemplo.cpp
//  C++ParaImpacientes
//
//  Created by Armando Galve Carbó on 18/10/13.
//  Copyright (c) 2013 Armando Galve Carbó. All rights reserved.
//

#include "ejecutarejemplo.h"
#include <vector>
using namespace std;

// esta funcion recibe como parametro un vector de funciones
// solicita al usuario el indice de la función a ejecutar

void ejecutarejemplo(vector<function< void()>> funciones ) {
    
    //vector<function< void()>> funciones=definiciones();
    
    long nfunciones= funciones.size();
    
    long indice=0,entrada;
    
    cout<<"Introducir un número, comprendido entre 0 y  " << nfunciones-1<<
    " , o un no dígito,  para ejecutar la última función  Entrada :"<<endl;
    
    if (cin>>entrada && entrada>-1 && entrada<nfunciones) {
        indice=entrada;
        cin.ignore();
        
    }
    else {
        cin.clear();
        cin.ignore();
        indice=nfunciones-1;
    }
    
    function <void()> f =funciones[indice];
    
    f();
    
    
}

