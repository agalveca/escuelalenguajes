//
//  ejecutarejemplo.h
//  C++ParaImpacientes
//
//  Created by Armando Galve Carbó on 18/10/13.
//  Copyright (c) 2013 Armando Galve Carbó. All rights reserved.
//

#ifndef __C__ParaImpacientes__ejecutarejemplo__
#define __C__ParaImpacientes__ejecutarejemplo__

#include <functional>
#include <iostream>
#include <vector>




using std::vector;
using std::function;

#endif /* defined(__C__ParaImpacientes__ejecutarejemplo__) */

void ejecutarejemplo(vector<function< void()>> funciones );
