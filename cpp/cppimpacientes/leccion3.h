//
//  Leccion3.h
//  C++ParaImpacientes
//
//  Created by Armando Galve Carbó on 18/10/13.
//  Copyright (c) 2013 Armando Galve Carbó. All rights reserved.
//

#ifndef __C__ParaImpacientes__Leccion3__
#define __C__ParaImpacientes__Leccion3__
#include <iostream>
#include <functional>

#include <vector>
#include <string>


#endif /* defined(__C__ParaImpacientes__Leccion3__) */

namespace leccion3 {
    
    
    std::vector<std::function <void()>> definiciones();
    
    
    void ejemplo1();
    void ejemplo2();
    void ejemplo3();
    
}
