//
//  Leccion3.cpp
//  C++ParaImpacientes
//
//  Created by Armando Galve Carbó on 18/10/13.
//
//  Leccion3.cpp
//  C++ParaImpacientes
//  Ejemplos de la tercera lección del libro
//  Created by Armando Galve Carbó on 08/10/13.
//
//Copyright (c) 2013 Armando Galve Carbó. All rights reserved.
//
#include "leccion3.h"

using std::vector;
using std::string;
using std::function;

namespace leccion3 {
    
    vector<function <void()>> definiciones() {
        
        vector<function< void()>> funciones={ ejemplo1, ejemplo2 , ejemplo3};
        //funciones.push_back([] () { cout << "Ejemplo de función lambda, lambda\n"; });
        return funciones;
        
    }
    
    // escribir un número en notación binaria
    // la explicación en c++ for the impatient
    
    void ejemplo1() {
        
        using std::cin;
        using std::cout;
        using std::endl;
        
        
        int n=0;
        unsigned short test_num=0;
        // la mascara en todo momento es 0000 1 000000
        // el 1 va cambiando de posición
        
        unsigned mask = 0x8000;
        
        cout<<" Introduce un número :"<< endl;
        cin>> test_num;
        
        while (n++< sizeof(test_num)*8) {
            
            cout << ((test_num & mask)!=0); // 1 o 0 si hay un digito donde la mascara tiene un 1
            
            //mask = mask>>1; //mueve el 1 de la mascara a la derecha
            
            mask>>=1;
            
            if (n % 4 ==0) { // cada 4 digitos un espacion
                cout << " ";
                
            }
            
        }
        
        cout<< endl;
        cin.ignore();
        cin.ignore();
        
        
        
        
        
        
    }
    
    void ejemplo2() {
        
        using std::cin;
        using std::cout;
        using std::endl;
        
        
        int n=0;
        unsigned int test_num=0;
        // la mascara en todo momento es 0000 1 000000
        // el 1 va cambiando de posición
        
        unsigned mask = 0x80000000;
        
        cout<<" Introduce un número :"<< endl;
        cin>> test_num;
        
        while (n++< sizeof(test_num)*8) {
            
            cout << ((test_num & mask)!=0); // 1 o 0 si hay un digito donde la mascara tiene un 1
            
            //mask = mask>>1; //mueve el 1 de la mascara a la derecha
            
            mask>>=1;
            
            if (n % 4 ==0) { // cada 4 digitos un espacion
                cout << " ";
                
            }
            
        }
        
        cout<< endl;
        cin.ignore();
        cin.ignore();
        
        
    }
    
    // acepta una cadena que representa un número binario
    // y devuelve su valor numérico
    // no chequea si la entrada es correcta
    
    void ejemplo3() {
        
        using std::cin;
        using std::cout;
        using std::endl;
        
        string input_string;
        int   output_number=0;
        int ndigitos=8;
        
        cout<<" Introduce una cadena de 8 digitos binarios 1 ó 0 :"<< endl;
        
        cin>> input_string;
        
        for (int i=0; i<ndigitos-1;i++) {
            
            if (input_string[i]=='1') output_number++;
            output_number*=2;
            
        }
        cout<< "El valor introducido es "<<output_number<< endl;
        
        cout<< endl;
        
        cin.ignore();
        cin.ignore();
        
        
        
        
        
    }
}

