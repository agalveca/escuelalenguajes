//
//  Leccion1.cpp
//  C++ParaImpacientes
//  Ejemplos de la primera lección del libro
//  Created by Armando Galve Carbó on 08/10/13.
//  Copyright (c) 2013 Armando Galve Carbó. All rights reserved.
//

#include "leccion1.h"
#include <iostream>
#include <vector>


using std::cin;
using std::cout;
using std::endl;


namespace leccion1 {
    
    vector<function <void()>> definiciones() {
        
        vector<function< void()>> funciones={
            
            ejemplo1,
            ejemplo2,
            ejemplo3,
            ejemplo4,
            ejemplo5,
            ejemplo6,
            ejemplo7
        
        };
        
        
        //funciones.push_back([] () { cout << "Ejemplo de función lambda, lambda\n"; });
        
        
        return funciones;
        
    }
    
    
    void ejemplo1() {
        // insert code here...
        cout << "Hola,mundo en c++!\n";
        cin.ignore();
        
    }
    
    void ejemplo2() {
        double x=0.0,y=0.0;
        cout<<"Introduce un valor de x ";
        cin>>x;
        cout<<"Introduci un valor de y ";
        cin>>y;
        cout<<"El promedio de los dos valores es "<<(x+y)/2<<endl;
        cout<<"Pulsa enter para salir ";
        cin.ignore();
        cin.ignore();
        
    }
    
    void ejemplo3() {
        double x=0.0,y=0.0;
        cout<<"Introduce un valor de x ";
        cin>>x;
        cout<<"Introduci un valor de y ";
        cin>>y;
        if (x>y) {
            cout <<"El valor máximo es "<< x <<endl;
        } else {
            cout <<"El valor máximo es "<< y <<endl;
        }
        
        cout<<"Pulsa enter para salir ";
        cin.ignore();
        cin.ignore();
        
    }
    
    void ejemplo4() {
        double x=0.0,y=0.0,max_valor;
        cout<<"Introduce un valor de x ";
        cin>>x;
        cout<<"Introduci un valor de y ";
        cin>>y;
        if (x>y) {
            max_valor=x;
            
        } else {
            max_valor=y;
            
        }
        cout <<"El valor máximo es "<< max_valor <<endl;
        cout<<"Pulsa enter para salir ";
        cin.ignore();
        cin.ignore();
        
    }
    
    void ejemplo5() {
        
        int suma=0,n=1;
        cout<<"Introducir una lista de números, terminada con 0";
        cout<<" o con un carácter que no sea dígito "<<"Entrada: ";
        
        while(n!=0){
            cin>>n;
            if(!cin) {
                n=0;
            }
            suma=suma+n;
            
        }
        
        cout<<"El total es "<<suma<<endl;
        cout<<"Pulsar Enter para salir ";
        cin.ignore();
        cin.ignore();
        
        
    }
    
    void ejemplo6() {
        
        int suma=0,n=1;
        cout<<"Introducir una lista de números, terminada con un no digito ";
        cout<<"Entrada: ";
        
        while(cin>>n){
            
            suma=suma+n;
            
        }
        
        cout<<"El total es "<<suma<<endl;
        cout<<"Pulsar Enter para salir ";
        cin.ignore();
        cin.ignore();
        
        
    }
    
    void ejemplo7() {
        
        double a=1.0,b=1.0,actual=0.0;
        int n=0;
        cout<<endl;
        cout<<"¿Cuántos elementos de la sucesión de Fibonacci deseas Enter: ";
        cin>>n;
        cout.precision(15);
        while(n>0) {
            actual=a+b;
            a=b;
            b=actual;
            cout<< actual <<"\t";
            cout<<" ratio = "<< b/a << endl;
            --n;
            
        }
        
        cout << endl;
        cout << "Pulsar Enter para salir "<< endl;
        cin.ignore();
        cin.ignore();
        
        
    }

    
}

