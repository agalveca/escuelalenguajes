package Slick.Mantenimiento





object Tables extends {
  val profile = scala.slick.driver.MySQLDriver
} with Tables



trait Tables {
  val profile: scala.slick.driver.JdbcProfile

  import profile.simple._

  import scala.slick.model.ForeignKeyAction

  import scala.slick.jdbc.{GetResult => GR}


  lazy val ddl = Averia.ddl ++ Equipo.ddl ++ Rol.ddl ++
    Ubicacion.ddl ++ Usuario.ddl ++ UsuarioUbicacion.ddl



  case class AveriaRow(id: Long,
                       version: Long,
                       asunto: String,
                       caracterAviso: String,
                       dateCreated: java.sql.Timestamp,
                       emisorId: Long,
                       equipoId: Option[Long],
                       estado: Option[String],
                       lastUpdated: java.sql.Timestamp,
                       texto: String,
                       tiempoEmpleado: Int,
                       tipo: String,
                       ubicacionId: Option[Long])



  implicit def GetResultAveriaRow(implicit e0: GR[Long],
                                  e1: GR[String],
                                  e2: GR[java.sql.Timestamp],
                                  e3: GR[Option[Long]],
                                  e4: GR[Option[String]],
                                  e5: GR[Int]): GR[AveriaRow] = GR{

    prs => import prs._
      AveriaRow.tupled((<<[Long], <<[Long], <<[String], <<[String], <<[java.sql.Timestamp], <<[Long], <<?[Long], <<?[String], <<[java.sql.Timestamp], <<[String], <<[Int], <<[String], <<?[Long]))
  }


  class Averia(tag: Tag) extends Table[AveriaRow](tag, "averia") {

    def * = (id, version, asunto, caracterAviso, dateCreated, emisorId, equipoId, estado, lastUpdated, texto, tiempoEmpleado, tipo, ubicacionId) <> (AveriaRow.tupled , AveriaRow.unapply)

    def ? = (id.?, version.?, asunto.?, caracterAviso.?,
      dateCreated.?, emisorId.?, equipoId, estado, lastUpdated.?,
      texto.?, tiempoEmpleado.?, tipo.?, ubicacionId).shaped <>
    ({r=>import r._; _1.map(_=> AveriaRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7, _8, _9.get, _10.get, _11.get, _12.get, _13)))},
      (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))


    val id: Column[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)

    val version: Column[Long] = column[Long]("version")

    val asunto: Column[String] = column[String]("asunto")

    val caracterAviso: Column[String] = column[String]("caracter_aviso")

    val dateCreated: Column[java.sql.Timestamp] = column[java.sql.Timestamp]("date_created")

    val emisorId: Column[Long] = column[Long]("emisor_id")

    val equipoId: Column[Option[Long]] = column[Option[Long]]("equipo_id")

    val estado: Column[Option[String]] = column[Option[String]]("estado")

    val lastUpdated: Column[java.sql.Timestamp] = column[java.sql.Timestamp]("last_updated")

    val texto: Column[String] = column[String]("texto")

    val tiempoEmpleado: Column[Int] = column[Int]("tiempo_empleado")

    val tipo: Column[String] = column[String]("tipo")

    val ubicacionId: Column[Option[Long]] = column[Option[Long]]("ubicacion_id")


    val index1 = index("FKAC348C3A2BEEE6BC", emisorId)

    val index2 = index("FKAC348C3A9669DBF7", equipoId)

    val index3 = index("FKAC348C3AC641519D", ubicacionId)

    val index4 = index("FKAC348C3AFD3FF0E2", emisorId)
  }

  lazy val Averia = new TableQuery(tag => new Averia(tag))


  case class EquipoRow(id: Long, version: Long, anoDeCompra: Option[String], caracteristicas: Option[String], codigo: String, descripcion: Option[String], descripcionUbicacion: String, discoDuro: Option[String], marca: Option[String], memoria: String, modelo: Option[String], red: Option[String], tipo: String, ubicacionId: Long)


  implicit def GetResultEquipoRow(implicit e0: GR[Long], e1: GR[Option[String]], e2: GR[String]): GR[EquipoRow] = GR{
    prs => import prs._
      EquipoRow.tupled((<<[Long], <<[Long], <<?[String], <<?[String], <<[String], <<?[String], <<[String], <<?[String], <<?[String], <<[String], <<?[String], <<?[String], <<[String], <<[Long]))
  }


  class Equipo(tag: Tag) extends Table[EquipoRow](tag, "equipo") {
    def * = (id, version, anoDeCompra, caracteristicas, codigo, descripcion, descripcionUbicacion, discoDuro, marca, memoria, modelo, red, tipo, ubicacionId) <> (EquipoRow.tupled, EquipoRow.unapply)

    def ? = (id.?, version.?, anoDeCompra, caracteristicas, codigo.?, descripcion, descripcionUbicacion.?, discoDuro, marca, memoria.?, modelo, red, tipo.?, ubicacionId.?).shaped.<>({r=>import r._; _1.map(_=> EquipoRow.tupled((_1.get, _2.get, _3, _4, _5.get, _6, _7.get, _8, _9, _10.get, _11, _12, _13.get, _14.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))


    val id: Column[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)

    val version: Column[Long] = column[Long]("version")

    val anoDeCompra: Column[Option[String]] = column[Option[String]]("ano_de_compra")

    val caracteristicas: Column[Option[String]] = column[Option[String]]("caracteristicas")

    val codigo: Column[String] = column[String]("codigo")

    val descripcion: Column[Option[String]] = column[Option[String]]("descripcion")

    val descripcionUbicacion: Column[String] = column[String]("descripcion_ubicacion")

    val discoDuro: Column[Option[String]] = column[Option[String]]("disco_duro")

    val marca: Column[Option[String]] = column[Option[String]]("marca")

    val memoria: Column[String] = column[String]("memoria")

    val modelo: Column[Option[String]] = column[Option[String]]("modelo")

    val red: Column[Option[String]] = column[Option[String]]("red")

    val tipo: Column[String] = column[String]("tipo")

    val ubicacionId: Column[Long] = column[Long]("ubicacion_id")


    val index1 = index("FKB2C89E3FC641519D", ubicacionId)
  }

  lazy val Equipo = new TableQuery(tag => new Equipo(tag))


  case class RolRow(id: Long, version: Long, aplicacion: String, nombre: String, usuarioId: Long)
  /** GetResult implicit for fetching RolRow objects using plain SQL queries */
  implicit def GetResultRolRow(implicit e0: GR[Long], e1: GR[String]): GR[RolRow] = GR{
    prs => import prs._
      RolRow.tupled((<<[Long], <<[Long], <<[String], <<[String], <<[Long]))
  }
  /** Table description of table rol. Objects of this class serve as prototypes for rows in queries. */
  class Rol(tag: Tag) extends Table[RolRow](tag, "rol") {
    def * = (id, version, aplicacion, nombre, usuarioId) <> (RolRow.tupled, RolRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (id.?, version.?, aplicacion.?, nombre.?, usuarioId.?).shaped <>({r=>import r._; _1.map(_=> RolRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id AutoInc, PrimaryKey */
    val id: Column[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column version  */
    val version: Column[Long] = column[Long]("version")
    /** Database column aplicacion  */
    val aplicacion: Column[String] = column[String]("aplicacion")
    /** Database column nombre  */
    val nombre: Column[String] = column[String]("nombre")
    /** Database column usuario_id  */
    val usuarioId: Column[Long] = column[Long]("usuario_id")

    /** Index over (usuarioId) (database name FK1B9CF545B8049) */
    val index1 = index("FK1B9CF545B8049", usuarioId)
    /** Index over (usuarioId) (database name FK1B9CF830A7623) */
    val index2 = index("FK1B9CF830A7623", usuarioId)
    /** Uniqueness Index over (usuarioId) (database name usuario_id) */
    val index3 = index("usuario_id", usuarioId, unique=true)
  }
  /** Collection-like TableQuery object for table Rol */
  lazy val Rol = new TableQuery(tag => new Rol(tag))




  case class UbicacionRow(id: Long, version: Long, identificador: String, nombre: String, restringida: Boolean, tipo: String)
  /** GetResult implicit for fetching UbicacionRow objects using plain SQL queries */
  implicit def GetResultUbicacionRow(implicit e0: GR[Long], e1: GR[String], e2: GR[Boolean]): GR[UbicacionRow] = GR{
    prs => import prs._
      UbicacionRow.tupled((<<[Long], <<[Long], <<[String], <<[String], <<[Boolean], <<[String]))
  }
  /** Table description of table ubicacion. Objects of this class serve as prototypes for rows in queries. */
  class Ubicacion(tag: Tag) extends Table[UbicacionRow](tag, "ubicacion") {
    def * = (id, version, identificador, nombre, restringida, tipo) <> (UbicacionRow.tupled, UbicacionRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (id.?, version.?, identificador.?, nombre.?, restringida.?, tipo.?).shaped <>({r=>import r._; _1.map(_=> UbicacionRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id AutoInc, PrimaryKey */
    val id: Column[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column version  */
    val version: Column[Long] = column[Long]("version")
    /** Database column identificador  */
    val identificador: Column[String] = column[String]("identificador")
    /** Database column nombre  */
    val nombre: Column[String] = column[String]("nombre")
    /** Database column restringida  */
    val restringida: Column[Boolean] = column[Boolean]("restringida")
    /** Database column tipo  */
    val tipo: Column[String] = column[String]("tipo")
  }
  /** Collection-like TableQuery object for table Ubicacion */
  lazy val Ubicacion = new TableQuery(tag => new Ubicacion(tag))






  case class UsuarioRow(id: Long, version: Long, apellido1: String, apellido2: String, nombre: String, password: String, user: String)
  /** GetResult implicit for fetching UsuarioRow objects using plain SQL queries */
  implicit def GetResultUsuarioRow(implicit e0: GR[Long], e1: GR[String]): GR[UsuarioRow] = GR{
    prs => import prs._
      UsuarioRow.tupled((<<[Long], <<[Long], <<[String], <<[String], <<[String], <<[String], <<[String]))
  }
  /** Table description of table usuario. Objects of this class serve as prototypes for rows in queries. */
  class Usuario(tag: Tag) extends Table[UsuarioRow](tag, "usuario") {
    def * = (id, version, apellido1, apellido2, nombre, password, user) <> (UsuarioRow.tupled, UsuarioRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (id.?, version.?, apellido1.?, apellido2.?, nombre.?, password.?, user.?).shaped <>({r=>import r._; _1.map(_=> UsuarioRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column id AutoInc, PrimaryKey */
    val id: Column[Long] = column[Long]("id", O.AutoInc, O.PrimaryKey)
    /** Database column version  */
    val version: Column[Long] = column[Long]("version")
    /** Database column apellido1  */
    val apellido1: Column[String] = column[String]("apellido1")
    /** Database column apellido2  */
    val apellido2: Column[String] = column[String]("apellido2")
    /** Database column nombre  */
    val nombre: Column[String] = column[String]("nombre")
    /** Database column password  */
    val password: Column[String] = column[String]("password")
    /** Database column user  */
    val user: Column[String] = column[String]("user")
  }
  /** Collection-like TableQuery object for table Usuario */
  lazy val Usuario = new TableQuery(tag => new Usuario(tag))




  case class UsuarioUbicacionRow(usuarioUbicacionesId: Option[Long], ubicacionId: Option[Long])
  /** GetResult implicit for fetching UsuarioUbicacionRow objects using plain SQL queries */
  implicit def GetResultUsuarioUbicacionRow(implicit e0: GR[Option[Long]]): GR[UsuarioUbicacionRow] = GR{
    prs => import prs._
      UsuarioUbicacionRow.tupled((<<?[Long], <<?[Long]))
  }
  /** Table description of table usuario_ubicacion. Objects of this class serve as prototypes for rows in queries. */
  class UsuarioUbicacion(tag: Tag) extends Table[UsuarioUbicacionRow](tag, "usuario_ubicacion") {
    def * = (usuarioUbicacionesId, ubicacionId) <> (UsuarioUbicacionRow.tupled, UsuarioUbicacionRow.unapply)

    /** Database column usuario_ubicaciones_id  */
    val usuarioUbicacionesId: Column[Option[Long]] = column[Option[Long]]("usuario_ubicaciones_id")
    /** Database column ubicacion_id  */
    val ubicacionId: Column[Option[Long]] = column[Option[Long]]("ubicacion_id")

    /** Index over (usuarioUbicacionesId) (database name FK5577C8CE20827495) */
    val index1 = index("FK5577C8CE20827495", usuarioUbicacionesId)
    /** Index over (ubicacionId) (database name FK5577C8CEC641519D) */
    val index2 = index("FK5577C8CEC641519D", ubicacionId)
    /** Index over (usuarioUbicacionesId) (database name FK5577C8CEF1D37EBB) */
    val index3 = index("FK5577C8CEF1D37EBB", usuarioUbicacionesId)
  }
  /** Collection-like TableQuery object for table UsuarioUbicacion */
  lazy val UsuarioUbicacion = new TableQuery(tag => new UsuarioUbicacion(tag))
}


