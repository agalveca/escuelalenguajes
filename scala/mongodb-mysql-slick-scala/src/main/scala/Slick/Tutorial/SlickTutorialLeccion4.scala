package Slick.Tutorial

import scala.slick.driver.MySQLDriver.simple._


object SlickTutorialLeccion4 extends App{

  val suppliers: TableQuery[Suppliers] = TableQuery[Suppliers]

  val coffees: TableQuery[Coffees] = TableQuery[Coffees]

  val url = "jdbc:mysql://localhost:3306/mantenimiento_rest"

  val user="usuariointraweb"

  val password="19601706"

  val driver = "com.mysql.jdbc.Driver"

  Database.forURL(url=url,driver=driver,user=user,password=password ).withSession {
    implicit session =>


    /* Query Composition */

      val composedQuery: Query[Column[String], String] =

             coffees.sortBy(_.name).take(3).filter(_.price > 9.0).map(_.name)

      println("Generated SQL for composed query:\n" + composedQuery.selectStatement)

      // Execute the composed query
      println(composedQuery.list)


      /* Joins */

      // Join the tables using the relationship defined in the Coffees table
      val joinQuery: Query[(Column[String], Column[String]), (String, String)] = for {
        c <- coffees if c.price > 9.0
        s <- c.supplier
      } yield (c.name, s.name)

      println("Generated SQL for the join query:\n" + joinQuery.selectStatement)

      // Print the rows which contain the coffee name and the supplier name
      println(joinQuery.list)


      /* Computed Values */

      // Create a new computed column that calculates the max price
      val maxPriceColumn: Column[Option[Double]] = coffees.map(_.price).max

      println("Generated SQL for max price column:\n" + maxPriceColumn.selectStatement)

      // Execute the computed value query
      println(maxPriceColumn.run)


      /* Manual SQL / String Interpolation */

      // Required import for the sql interpolator
      import scala.slick.jdbc.StaticQuery.interpolation

      // A value to insert into the statement
      val state = "CA"

      // Construct a SQL statement manually with an interpolated value
      val plainQuery = sql"select SUP_NAME from suppliers where STATE = $state".as[String]

      println("Generated SQL for plain query:\n" + plainQuery.getStatement)

      // Execute the query
      println(plainQuery.list)



  }

}
