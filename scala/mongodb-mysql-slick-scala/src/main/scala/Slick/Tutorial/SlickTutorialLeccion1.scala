package Slick.Tutorial

import scala.slick.driver.MySQLDriver.simple._

object SlickTutorialLeccion1 extends App{

  val suppliers: TableQuery[Suppliers] = TableQuery[Suppliers]

  val coffees: TableQuery[Coffees] = TableQuery[Coffees]

  val url = "jdbc:mysql://localhost:3306/mantenimiento_rest?user=usuariointraweb&password=19601706"

  val driver = "com.mysql.jdbc.Driver"

  Database.forURL(url=url,driver=driver ).withSession {
    implicit session =>


    (suppliers.ddl ++ coffees.ddl).create

    // (suppliers.ddl ++ coffees.ddl).drop


    suppliers += (101, "Acme, Inc.", "99 Market Street", "Groundsville", "CA", "95199")
    suppliers += (49, "Superior Coffee", "1 Party Place", "Mendocino", "CA", "95460")
    suppliers += (150, "The High Ground", "100 Coffee Lane", "Meadows", "CA", "93966")


    val resultadoInsertarCoffees: Option[Int] = coffees ++= Seq (
      ("Colombian",         101, 7.99, 0, 0),
      ("French_Roast",       49, 8.99, 0, 0),
      ("Espresso",          150, 9.99, 0, 0),
      ("Colombian_Decaf",   101, 8.99, 0, 0),
      ("French_Roast_Decaf", 49, 9.99, 0, 0)
    )




    resultadoInsertarCoffees foreach (numRows => println(s"Insertados $numRows filas en la tabla Coffees "))


    println("SQL Generado para la base de datos Coffees:\n" + coffees.selectStatement)




}
}
