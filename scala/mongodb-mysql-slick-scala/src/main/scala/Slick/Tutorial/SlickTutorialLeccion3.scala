package Slick.Tutorial

import scala.slick.driver.MySQLDriver.simple._


object SlickTutorialLeccion3 extends App{

  val suppliers: TableQuery[Suppliers] = TableQuery[Suppliers]

  val coffees: TableQuery[Coffees] = TableQuery[Coffees]

  val url = "jdbc:mysql://localhost:3306/mantenimiento_rest"

  val user="usuariointraweb"

  val password="19601706"

  val driver = "com.mysql.jdbc.Driver"

  Database.forURL(url=url,driver=driver,user=user,password=password ).withSession {
    implicit session =>


      val filterQuery:      Query[Coffees, (String, Int, Double, Int, Int)] = coffees.filter(_.price > 9.0)
      val updateQuery:      Query[Column[Int], Int] = coffees.map(_.sales)
      val deleteQuery:      Query[Coffees,(String, Int, Double, Int, Int)]  = coffees.filter(_.price < 8.0)
      val justNameQuery:    Query[Column[String], String]                   = coffees.map(_.name)
      val sortByPriceQuery: Query[Coffees, (String, Int, Double, Int, Int)] = coffees.sortBy(_.price)

      println("SQL generado para consulta filtrada :\n" +                 filterQuery.selectStatement)

      println("Sql generado para consulta de actualización Coffees :\n" + updateQuery.updateStatement)

      println("SQl generado para borrado de  :\n" +                       deleteQuery.deleteStatement)

      println("SQl generado para consulta que retorna solo el nombre:\n" + justNameQuery.selectStatement)

      println(" SQl generado para consulta ordenada por precio:\n"       + sortByPriceQuery.selectStatement)


      println(filterQuery.list)

      val numUpdatedRows = updateQuery.update(1)

      println(s"Updated $numUpdatedRows rows")


      val numDeletedRows = deleteQuery.delete

      println(s"Deleted $numDeletedRows rows")

      println(justNameQuery.list)


      println(sortByPriceQuery.list)



  }

}
