package Slick.Tutorial

import scala.slick.driver.MySQLDriver.simple._


object SlickTutorialLeccion2 extends App{

  val suppliers: TableQuery[Suppliers] = TableQuery[Suppliers]

  val coffees: TableQuery[Coffees] = TableQuery[Coffees]

  val url = "jdbc:mysql://localhost:3306/mantenimiento_rest"

  val user="usuariointraweb"

  val password="19601706"

  val driver = "com.mysql.jdbc.Driver"

  Database.forURL(url=url,driver=driver,user=user,password=password ).withSession {
    implicit session =>





      coffees foreach { case (name, supID, price, sales, total) =>
        println("  " + name + "\t" + supID + "\t" + price + "\t" + sales + "\t" + total)
      }

      // la TableQuery no tiene el método map

      val todosProveedores: List[(Int, String, String, String, String, String)] = suppliers.list

      val texto= todosProveedores.map { case (id, nombre,calle,ciudad,estado,codigo) =>
      "id: " +   id+" nombre:  " + nombre + " calle: " + calle } mkString "\n"

      println(texto)



  }

}
