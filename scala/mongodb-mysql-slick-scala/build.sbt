name := "MongoDBDemo1"

version := "1.0"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
"org.mongodb" %% "casbah" % "2.7.0-RC2",
"org.slf4j" % "slf4j-simple" % "1.6.4",
"com.typesafe.slick" %% "slick" % "2.0.1-RC1",
"mysql" % "mysql-connector-java" % "5.1.29"
)






