# http://learnxinyminutes.com/docs/es-es/ruby-es/
=begin
Ejemplos obtenidos en la página del enlace, para el aprendizaje de ruby
rspec leccion1_test.rb

Debido a un problema del terminal gnome solo funciona en el perfil nocturno
=end

describe "Learn Ruby en Y minutos" do 

	before do
		puts "Tests rspec leccion de ruby"
	end

	it "todo es un objeto" do

    calculos = [3.class, 3.to_s, 'I am a string'.class,"I am a string too".class,:pendiente.class ]

    resultados= [Fixnum,"3",String,String, Symbol]

    expect(  calculos).to eq (resultados)

	end

	it "Aritmetica basica" do

	calculos= [1 + 1,	8 - 1, 10 * 2, 35 / 5,1.+(3),10.*(5) ]

  resultados= [2,7,20,7,4,50]

  expect(  calculos).to eq (resultados)

	end

	it "Operaciones logicas " do

    comparaciones=
      [ 1 == 1, 2 == 1,1 != 1 , 2 != 1 , !true  , !false, !nil  , !false , !0,1 < 10 , 1 > 10 , 2 <= 2 , 2 >= 2,
        :pendiente == 'pendiente']

    resultados = [true,false,false,true,false,true,true,true,false,true,false,true,true,false]

    expect(comparaciones).to eq resultados

  end

  it "Interpolacion " do

    placeholder = "usar interpolacion de cadenas"

     "Se puede #{placeholder} cuando se usan cadenas con doble comilla".should eq (
    "Se puede usar interpolacion de cadenas cuando se usan cadenas con doble comilla"   )

  end

  it "Doble asignacion " do
    x=y=10
    x.should eq 10
    y.should eq 10
  end

	
end