describe "Arrays" do

	before do
		puts "Estructuras de datos"
    array=[1,"Hello",false ]
	end

	it "puede tener elementos de tipos diferentes" do

    array=[1,"Hello",false ]

    expect(array.class).to eq Array

  end


  it "Se accede a los elementos con [], comenzando en 0" do

  array = [1, 2, 3, 4, 5]

  calculo=[array[0],array[1],array.[](2), array.[](3),array[-1]]

  expect(calculo).to eq [1,2,3,4,5]

  end

  it "Se accede a rangos " do
    array = [1, 2, 3, 4, 5]
    # With a start index and length
    array[2, 3].should eq  [3, 4, 5]

    # Or with a range
    array[1..3].should eq  [2, 3, 4]


  end

  it "Se añaden elementos " do
    array = [1, 2, 3, 4, 5]
    # Add to an array like this
        expect(array << 6).to eq ([1, 2, 3, 4, 5, 6] )

  end



end