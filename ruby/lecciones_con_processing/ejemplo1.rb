#Getting Started with Processing pag10
require 'ruby-processing'

class Ejemplo < Processing::App


  def setup
    size 480, 400
    smooth
  end

  def draw
    if mousePressed
      fill 0
    else
      fill 255
    end
    ellipse mouseX, mouseY, 80, 80
  end


end



Ejemplo.new(:width => 480, :height => 800, :title => "Ejemplo", :full_screen => false)





