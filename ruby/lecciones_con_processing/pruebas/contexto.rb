require 'java'
java_import 'javax.swing.JFrame'
java_import 'java.awt.BorderLayout'

module Contexto
class Marco < JFrame

    def initialize(paplet)
        super "Programa processing"
        self.initUI(paplet)
    end

    def initUI(paplet)

        self.setSize 600, 600
        self.setDefaultCloseOperation JFrame::EXIT_ON_CLOSE
        self.layout = BorderLayout.new
        paplet.init
        self.add paplet,BorderLayout::CENTER

        self.setLocationRelativeTo nil
        self.setVisible true
    end
end

class FrameProcessing < JFrame

    def initialize(clase,dimensiones)
        super "Processing en JRuby"
        @instancia=clase.new
        ancho=dimensiones[:ancho] || 600
        alto = dimensiones[:alto] || 600
        initUI ancho, alto

    end

    def initUI(ancho,alto)

        setSize ancho, alto
        setDefaultCloseOperation JFrame::EXIT_ON_CLOSE
        layout = BorderLayout.new
        @instancia.init
        add @instancia,BorderLayout::CENTER

        setLocationRelativeTo nil
        setVisible true
    end
end


end