require 'java'

java_import 'javax.swing.JFrame'

module Processing
  include_package "processing.core"
end

class Ejemplo < Processing::PApplet


	def setup
         size 400, 400
         stroke 255
         background 192, 64, 0
	end

   def draw
         line(150, 25, mouseX, mouseY);
      end

    def mousePressed
       background(192, 64, 0)
  end


end




class Programa < JFrame

    def initialize
        super "Programa processing"
        self.initUI
    end

    def initUI

        self.setSize 400, 400
        self.setDefaultCloseOperation JFrame::EXIT_ON_CLOSE

        ejemplo=   Ejemplo.new
        ejemplo.init
        self.add ejemplo

        self.setLocationRelativeTo nil
        self.setVisible true
    end
end

Programa.new





