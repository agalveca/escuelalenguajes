require 'contexto'

module Processing
  include_package "processing.core"
end

class Ejemplo < Processing::PApplet

  def setup
    size 400, 400
    stroke 255
    background 192, 64, 0
  end

  def draw
    line(150, 25, mouseX, mouseY);
  end

  def mousePressed
    background(192, 64, 0)
  end

end

Contexto::Marco.new(Ejemplo.new)


