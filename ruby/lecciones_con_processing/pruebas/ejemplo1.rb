#Getting Started with Processing pag10
require_relative 'contexto'

module Processing
  include_package "processing.core"
end

class Ejemplo < Processing::PApplet


  def setup
    size 480, 400
    smooth
  end

  def draw
    if mousePressed
      fill 0
    else
      fill 255
    end
    ellipse mouseX, mouseY, 80, 80
  end


end

Contexto::FrameProcessing.new(Ejemplo,ancho:480,alto:400)




