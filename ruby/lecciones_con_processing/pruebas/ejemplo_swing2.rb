include Java
import javax.swing.JFrame
import javax.swing.UIManager
frame = JFrame.new "JRuby Look And Feel"
frame.default_close_operation = JFrame::EXIT_ON_CLOSE
frame.content_pane.layout = java.awt.GridLayout.new(1, 2)
{:metal => "javax.swing.plaf.metal.MetalLookAndFeel",
 :system => UIManager::getSystemLookAndFeelClassName}.each do |l, c|
  but = javax.swing.JButton.new l.to_s
  but.add_action_listener do |evt|
    UIManager::look_and_feel = c javax.swing.SwingUtilities::updateComponentTreeUI
    frame
    frame.pack
  end
  frame.add(but)
end
frame.pack frame.visible = true