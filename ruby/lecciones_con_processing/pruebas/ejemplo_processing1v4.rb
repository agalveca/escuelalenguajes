require 'contexto'

module Processing
  include_package "processing.core"
end

class Ejemplo < Processing::PApplet

  def setup
    size width, height
    smooth
    background  0
    mouseX=width/2
    mouseY=height/2
  end

  def draw
    r=random 200
    fill 0,0,random(255)
    ellipse(mouseX,mouseY,r,r)
  end

  def mousePressed
      background 0
  end

end

Contexto::Marco.new(Ejemplo.new)


