#The Nature of code
require_relative 'contexto'


java_import 'processing.core.PApplet'
java_import 'processing.core.PConstants'

class Walker
  attr_reader :x, :y

  def initialize(padre)
    @x=padre.width/2
    @y=padre.height/2
    @padre=padre
  end


  # Randomly move up, down, left, right, or stay in one place
  def walk
    vx = @padre.random -2, 2
    vy = @padre.random -2, 2
    @x += vx
    @y += vy

    # Stay on the screen
    @x = PApplet.constrain(@x, 0, @padre.width-1)
    @y = PApplet.constrain(@y, 0, @padre.height-1)


  end

  def render
    @padre.stroke 0
    @padre.fill 175
    @padre.rect_mode PConstants.CENTER
    @padre.rect @x, @y, 40, 40
  end
end

class Ejemplo < PApplet


  def setup
    size 400, 400
    frameRate 30
    @w=Walker.new(self)

  end

  def draw
    background 255

    @w.walk
    @w.render
  end


end

Contexto::FrameProcessing.new(Ejemplo, ancho: 480, alto: 400)