#The Nature of code
require_relative 'contexto'


java_import 'processing.core.PApplet'
java_import 'processing.core.PConstants'

# Se abre la clase para poder acceder a sus metodos
# sin necesidad de usar una instancia padre
# como en la versión a

class PApplet
  attr_accessor :walkerx, :walkery


  def inicializar_walker
    @walkerx=width/2
    @walkery=height/2
  end


  def walk
    vx = random -2, 2
    vy = random -2, 2
    @walkerx += vx
    @walkery += vy

    @walkerx = PApplet.constrain(walkerx, 0, width-1)
    @walkery = PApplet.constrain(walkery, 0, height-1)


  end

  def walker_render
    stroke 0
    fill 175
    rect_mode PConstants.CENTER
    rect walkerx, walkery, 40, 40
  end
end

class Ejemplo < PApplet


  def setup
    size 400, 400
    frameRate 30
    inicializar_walker
  end

  def draw
    background 255
    walk
    walker_render
  end


end

Contexto::FrameProcessing.new(Ejemplo, ancho: 480, alto: 400)