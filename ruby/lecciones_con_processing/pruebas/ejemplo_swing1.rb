#! /usr/local/bin/jruby -w

  require 'java'

  include_class 'java.awt.event.ActionListener'
  include_class 'javax.swing.JButton'
  include_class 'javax.swing.JFrame'

  class ClickAction < ActionListener
  def actionPerformed(event)
     puts "Button got clicked."
   end
 end

 class MainWindow < JFrame
   def initialize
     super "JRuby/Swing Demo"
     setDefaultCloseOperation JFrame::EXIT_ON_CLOSE

     button = JButton.new "Click me!"
     button.addActionListener ClickAction.new
     add button
     pack
   end
 end

 MainWindow.new.setVisible true