#The Nature of code
require 'ruby-processing'

class Walker
  attr_reader :x, :y

  def initialize(padre)
    @x=padre.width/2
    @y=padre.height/2
    @padre=padre
  end


  # Randomly move up, down, left, right, or stay in one place
  def step
    vx = (@padre.random(3).to_int )-1
    vy = (@padre.random(3).to_int) -1
    @x += vx
    @y += vy

    # Stay on the screen
    @x = PApplet.constrain(@x, 0, @padre.width-1)
    @y = PApplet.constrain(@y, 0, @padre.height-1)


  end

  def render
    @padre.stroke 255

    @padre.point @x, @y
  end
end

class Ejemplo < Processing::App


  def setup
    size 400, 400
    background 0
    @w=Walker.new(self)

  end

  def draw


    @w.step
    @w.render
  end


end



Ejemplo.new(:width => 480, :height => 400, :title => "Camino aleatorio", :full_screen => false)