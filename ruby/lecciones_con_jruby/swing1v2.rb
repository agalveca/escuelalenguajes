# http://zetcode.com/gui/jrubyswing/introduction/
require 'java'

java_import 'javax.swing.JFrame'


class Example < JFrame

    def initialize
        super "Simple version 2"
        self.initUI
    end

    def initUI

        self.set_size 300, 200

        self.set_default_close_operation JFrame::EXIT_ON_CLOSE
        self.set_location_relative_to  nil
        self.set_visible true
    end
end

Example.new