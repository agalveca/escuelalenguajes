#!/usr/local/bin/jruby

# ZetCode JRuby Swing tutorial
#
# This program creates a quit
# button. When we press the button,
# the application terminates.
#
# author: Jan Bodnar
# website: www.zetcode.com
# last modified: December 2010

include Java

import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JPanel
import java.lang.System


class Example < JFrame

    def initialize
        super "Quit button"

        self.init_ui
    end

    def init_ui

        panel = JPanel.new
        self.get_content_pane.add panel

        panel.set_layout nil

        qbutton = JButton.new "Salir"
        qbutton.set_bounds 50, 60, 80, 30
        qbutton.add_action_listener do |e|
            System.exit 0
        end

        panel.add qbutton

        self.set_default_close_operation JFrame::EXIT_ON_CLOSE
        self.set_size 300, 200
        self.set_location_relative_to nil
        self.set_visible true
    end

end

Example.new