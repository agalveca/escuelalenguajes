#! /usr/local/bin/jruby -w

  require 'java'

  classes = %w(JFrame JButton)

  classes.each do |c|
    java_import "javax.swing.#{c}"
  end

  awt=%w(Dimension event.ActionListener)

  awt.each do |c|
      java_import "java.awt.#{c}"
    end


  #implementar el interface

  class ClickAction
    include ActionListener

    def action_performed(event)
     puts "Se ha pulsado el boton."
   end
 end

 class MainWindow < JFrame

   def initialize
     super "JRuby/Swing Demo"

     set_default_close_operation JFrame::EXIT_ON_CLOSE
     button = JButton.new "Pulsame!"
     button.add_action_listener ClickAction.new
     add button
     pack
   end
 end

 MainWindow.new.setVisible true