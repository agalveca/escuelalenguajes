#!/usr/bin/env node

// Ver http://mongodb.github.io/node-mongodb-native/

var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://127.0.0.1:27017/contacto', function(err, db) {

    if (err) throw err;

    var documento = {'name': 'Ricardo Corazon', 'phone': 1234567};

    var collection = db.collection('contacto');

    collection.insert(documento, {w: 1},
        function (error, registros) {

            if (error) throw error;

            console.log("Registro añadido  " + registros[0]._id);
        }
    );

    collection.find({}).toArray(

        function(error,items) {
            console.log(items);


        }
    );



    collection.findOne({},function(err,item) {
        if (err) return(error);

        console.dir("Ahora encontrar uno");
        console.log(item);

    });

    var stream= collection.find().stream();
    stream.on("data",function (item) {
        console.log("Un item");
        console.log(item);
    });

    stream.on("end",function() {
        console.log("FIN");
    })
});


console.dir("Terminado!");