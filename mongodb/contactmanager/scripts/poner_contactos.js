#!/usr/bin/env mongo

// Fichero mongo para añadir contactos a la base de datos contact

// ver http://docs.mongodb.org/manual/tutorial/write-scripts-for-the-mongo-shell/

db = connect("localhost:27017/contact");

db.contact.insert({'name' : 'Juan Sin Tierra', 'phone' : 1234567});

var docs = db.contact.find({});

// Print the result
printjson(docs);


