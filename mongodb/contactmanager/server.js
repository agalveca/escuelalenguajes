


var express = require("express");  
var contacts = require("./contact");  
var app = express();


app.configure(function () {  
  app.use(express.bodyParser());
});

app.get("/", contacts.index);  
app.get('/contacts', contacts.index);  
app.get('/contacts/:id', contacts.findById); 
app.post('/contacts', contacts.addContact);  
app.put('/contacts/:id', contacts.updateContact);  
app.delete('/contacts/:id', contacts.deleteContact);  

//app.listen(1222, "127.0.0.1");  

app.listen(1222, function() {
    console.log('Aplicación a la escucha en 1222');
});

