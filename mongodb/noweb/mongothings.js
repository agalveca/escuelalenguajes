#!/usr/bin/env mongo

db.things.insert({'a' : 1, 'b' : 2})

var doc = db.things.findOne();

// Print the result
printjson(doc);