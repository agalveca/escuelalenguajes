#!/usr/bin/env node



var mongodb = require('mongodb');


var server = new mongodb.Server("127.0.0.1", 27017, {});


var dbTest = new mongodb.Db('unTestDB', server, {})


dbTest.open(function (error, client) {
    if (error) throw error;

    //en el parámetro client recibimos el cliente para comenzar a hacer llamadas
    //este parámetro sería lo mismo que hicimos por consola al llamar a mongo

    //Obtenemos la coleccion personas que creamos antes

    var collection = new mongodb.Collection(client, 'personas');

    //disparamos un query buscando la persona que habiamos insertado por consola
    collection.find({'nombre': 'pepe'}).toArray(function(err, docs) {

        //imprimimos en la consola el resultado
        console.dir(docs);
    });
});
