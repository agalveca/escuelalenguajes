// super-cli.js

var MongoClient = require("mongodb").MongoClient



var uri="mongodb://localhost:27017/pruebas"


function getConnection(cb) {

    MongoClient.connect(uri, function(err, db) {

        if (err) return cb(err)

        var superheroes = db.collection("superheroes")

        // because we are searching by name, we need an index! without an index, things can get slow
        // if you want to learn more: http://docs.mongodb.org/manual/core/indexes-introduction/

        superheroes.ensureIndex({name: true}, function(err) {
            if (err) return cb(err)
            cb(null, superheroes)
        })
    })
}

// an upsert will create a new record OR update an existing record, which makes things easier
// in mongo, we can do this with a findAndModify and passing the upsert option
// to have the updated document returned, we pass the new option as well
function upsertPower(collection, name, power, cb) {
    collection.findAndModify({name: name}, {}, {$set: {superpower: power}}, {upsert: true, new: true}, cb)
}

// instead of finding just one hero, we can list all of the
// documents by passing an empty selector.
// This returns a 'cursor', which allows us to walk through the documents
// look at how we do this in processHeroes
function readAll(collection, cb) {
    collection.find({}, cb)
}

function readPower(collection, name, cb) {
    collection.findOne({name: name}, cb)
}

function printHero(hero) {
    // make sure we found our hero!
    if (!hero) {
        console.log("couldn't find the hero you asked for!")
    }
    console.log(hero.name + " has the power of " + hero.superpower)
}

// the each method allows us to walk through the result set, notice the callback, as every time the callback
// is called, there is another chance of an error
function printHeroes(heroes, cb) {
    heroes.each(function(err, hero) {
        if (err) return cb(err)
        printHero(hero)
    })
}

function superPowerCli(operation, name, superpower, cb) {

    getConnection(function(err, collection) {
        if (err) return cb(err)

        // we need to make sure to close the database, otherwise, the process
        // won't stop
        function processHero(err, hero) {
            if (err) return cb(err)
            printHero(hero)
            collection.db.close()
            cb()
        }

        // use this function when dealing with lots of heroes
        function processHeroes(err, heroes) {
            if (err) return cb(err)
            // the callback to each is called for every result, once it returns a null, we know
            // the result set is done
            heroes.each(function(err, hero) {
                if (err) return cb(err)
                if (hero) {
                    printHero(hero)
                } else {
                    collection.db.close()
                    cb()
                }
            })
        }

        if (operation === "list") {
            readAll(collection, processHeroes)
        } else if (operation === "update") {
            upsertPower(collection, name, superpower, processHero)
        } else if (operation === "read" ){
            readPower(collection, name, processHero)
        } else {
            return cb(new Error("unknown operation!"))
        }
    })
}

var operation = process.argv[2]
var name = process.argv[3]
var superpower = process.argv[4]

superPowerCli(operation, name, superpower, function(err) {
    if (err) {
        console.log("had an error!", err)
        process.exit(1)
    }
})

/*

 Now, inter­action!:

 node super-cli.js update Addison puns

 node super-cli.js update Ian coding

 node super-cli read Addison

 node super-cli list


 */