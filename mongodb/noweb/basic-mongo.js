
// basic-mongo.js

var MongoClient = require("mongodb").MongoClient

var uri_="mongodb://addison:supersecure@paulo.mongohq.com:10000/superhero"
var uri="mongodb://localhost:27017/pruebas"
MongoClient.connect(uri, function(err, db) {


    console.log("Conexión establecida");
    if (err) throw err


    var superheroes = db.collection("superheroes")

    superheroes.insert({name: "Addison", superpower: "insert"},

        function(err, result) {
            if (err) throw err


            var _id = result[0]._id


            superheroes.update({_id: _id}, {$set: {superpower: "update"}}, function(err) {
                if (err) throw err


                superheroes.findOne({_id: _id}, function(err, doc) {
                    if (err) throw err
                    console.log(doc.name + " has the power to " + doc.superpower)

                    db.close()
                })
            })

        })
})