#Instalación de Mongodb
## En CENTOS

Se crea un archivo 10gen.repo en /etc/yum.repos.d con el siguiente contenido

```
[10gen]
name=10gen Repository
baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64
gpgcheck=0
enabled=1
```

a continuación `yum update`   y `yum install mongo-10gen mongo-10gen-server`

La instalación ocupa unos 85 Mb. Los binarios están en /usr/bin

Se crea `/etc/rc.d/init.d/mongod `

Los datos se guardan en `/var/lib/mongo` , los  log  en `/var/log/mongo`

EL propietario de estos directorios es un usuario `mongod`

Se arranca el servicio mediante `service mongod start`

Se pueden consultar los logs en `/var/log/mongo/mongod.log`

Para que se arranque el servicio automáticamente `chkconfig mongod on`

###Clientes gráficos

umongo, instalado en ~/opt

##En Mac 

Se descarga la última versión, se extrae y, opcionalmente, se copia en otra ubicación

	curl -O http://downloads.mongodb.org/osx/mongodb-osx-x86_64-2.4.9.tgz
	tar -zxvf mongodb-osx-x86_64-2.4.9.tgz
	
	mkdir -p mongodb
	cp -R -n mongodb-osx-x86_64-2.4.9/ mongodb
	
Alternativamente  `mv mongodb-osx-x86_64-2.4.9 /usr/local/mongodb`

Los datos se guardan en `/data/db` por ello

	 sudo mkdir -p /data/db
	$ whoami
	agc
	$ sudo chown agc /data/db

los permisos son necesarios para que el demonio pueda escribir en el directorio
   
NO Se edita `/etc/paths` para añadir `/usr/local/mongodb`
    
Se edita `.bash_profile` añadiendo
    
    	export MONGO_PATH=/usr/local/mongodb

		export PATH=${MONGO_PATH}/bin:$PATH
		

Comprobación de que se ha instalado correctamente

	Nova:~ agc$ mongo -version
    MongoDB shell version: 2.4.9


##Comienzo de MongoDb

Una sesión inicial

	$ mongod
	MongoDB starting : pid=34022 port=27017 dbpath=/data/db/ 64-bit host=mkyong.local
	//...
	waiting for connections on port 27017
	
si no se quiere usar el directorio de datos $ mongod --dbpath /any-directory

Para evitar el mensaje de error ` ulimit -n 2048 && mongod;`
	
	$ mongo
    MongoDB shell version: 2.2.3
	connecting to: test
	> show dbs
	local	(empty)

## Autostart

 `sudo nano /Library/LaunchDaemons/mongodb.plist`
 
 El contenido del archivo anterior
 
 
 	<?xml version="1.0" encoding="UTF-8"?>
	<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
	<plist version="1.0">
	<dict>
  	<key>Label</key>
  	<string>mongodb</string>
  	<key>ProgramArguments</key>
  	<array>
    <string>/usr/local/mongodb/bin/mongod</string>
  	</array>
  	<key>RunAtLoad</key>
  	<true/>
  	<key>KeepAlive</key>
  	<true/>
  	<key>WorkingDirectory</key>
  	<string>/usr/local/mongodb</string>
  	<key>StandardErrorPath</key>
  	<string>/var/log/mongodb/error.log</string>
  	<key>StandardOutPath</key>
  	<string>/var/log/mongodb/output.log</string>
	</dict>
	</plist>


se lanza mediante

	sudo launchctl load /Library/LaunchDaemons/mongodb.plist
 
	$ ps -ef | grep mongo
	
El problema de los límites:

Se comprueban mediante `sudo launchctl limit`

Otra versión

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
  <key>Disabled</key>
  <false/>
  <key>Label</key>
  <string>org.mongo.mongod</string>
  <key>ProgramArguments</key>
  <array>
    <string>/usr/local/lib/mongodb/bin/mongod</string>
    <string>--dbpath</string>
    <string>/Users/Shared/mongodata/</string>
    <string>--logpath</string>
    <string>/var/log/mongodb.log</string>
  </array>
  <key>QueueDirectories</key>
  <array/>
  <key>RunAtLoad</key>
  <true/>
  <key>UserName</key>
  <string>daemon</string>
  <key>SoftResourceLimits</key>
  <dict>
    <key>NumberOfFiles</key>
    <integer>1024</integer>
    <key>NumberOfProcesses</key>
    <integer>512</integer>
  </dict>
</dict>
</plist>
```

He añadido, al anterior, el código siguiente


	<key>SoftResourceLimits</key>
	<dict>
        <key>NumberOfFiles</key>
        <integer>1024</integer>
	</dict>
##Clientes

`http://robomongo.org`

`https://github.com/fotonauts/MongoHub-Mac`

`http://edgytech.com/umongo/`



	




